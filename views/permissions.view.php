<?php
	require_once MODEL_PATH . 'template_functions.php';
	global $USERS;
	global $PLUGINS;
	$roles = $USERS->get_roles();
?>
<table id="permissions" class="table table-striped">
	<thead>
		<th>Permissions per plugin</th>
		<?php foreach ($roles as $role): ?>
			<th><?=$role?></th>
		<?php endforeach ?>
	</thead>
	<tbody>
		<?php foreach ($PLUGINS->get_active_plugins() as $plugin): ?>
			<?php $permissions = Database::get_manager()->select_quick(array('plugin' => $plugin), 'permission'); ?>
			<?php if (isset($permissions)): ?>
				<tr>
					<td colspan="<?=count($roles) + 1?>"><strong><?=$plugin?></strong></td>
				</tr>
				<?php foreach ($permissions as $permission): ?>	
					<tr>
						<td>
							<span class="name"><?=$permission['name']?></span>
							<?='<small>' . @$permission['description'] . '</small>'?>
						</td>
						<?php foreach ($roles as $rid => $role): ?>
							<td>
								<?php 
									$role_permission = Database::get_manager()->select_quick(array('pid' => $permission['pid'], 'rid' => $rid), 'role_permission');
									$checked = (isset($role_permission) ? 'checked' : '');
								?>
								<input type="checkbox" class="permission-role-check" name="role-<?=$rid?>-perm-<?=$permission['pid']?>" value="role-<?=$rid?>-perm-<?=$permission['pid']?>" <?=$checked?>>
							</td>
						<?php endforeach ?>
					</tr>
				<?php endforeach ?>
			<?php endif ?>
		<?php endforeach ?>
	</tbody>
</table>