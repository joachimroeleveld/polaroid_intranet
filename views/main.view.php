<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?=FILE_URL?>images/favicon.ico">
    <title><?=$this->title;?> | <?=$GLOBALS['config']['brand_name']?> intranet</title>
    <link rel="icon" type="image/png" href="">
	<style type="text/css">
		<?=@$this->stylesheets;?>
	</style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title><?=@$this->head;?></title>
</head>
<body class="<?php print((isset($this->bodyClass) ? ($this->bodyClass . ' ') : '') . sanitize($this->title, true, true)); ?>">
	<div class="l-page">
		<div class="l-header">
			<div class="l-navigation">
				<?php include('menu.view.php'); ?>
			</div>
			<div class="l-title">
				<h1><?=$this->title;?></h1>
			</div>
		</div>
		<div class="l-main">
			<div id="message-container"></div>
			<div class="l-content">
				<?=$this->body;?>
			</div>
		</div>
		<div class="l-footer"></div>
	</div>
	<?=@$this->scripts;?>
</body>
<!-- 

/##########################################################################\
| Developed with passion by Joachim Roeleveld - joachimroeleveld@gmail.com |
\##########################################################################/

-->
</html>