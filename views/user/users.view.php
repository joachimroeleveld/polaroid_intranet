<?php
	global $USERS;
	$users = $USERS->get_users();
	$roles = $USERS->get_roles();
?>
<!-- Add user functionality -->
<button type="button" class="btn btn-default add-user" data-action="add" data-toggle="modal" data-target="#add-user-modal">
	<span class="glyphicon glyphicon-plus"></span> 
	Add user
</button>
<div class="modal fade" id="add-user-modal" tabindex="-1" role="dialog" aria-labelledby="add-user" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Add user</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
			<div class="form-group">
			  <label class="col-md-4 control-label" for="add-user-username">Username</label>  
			  <div class="col-md-4">
			  	<input id="add-user-username" name="add-user-username" type="text" placeholder="username" class="form-control input-md" required>
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-md-4 control-label" for="add-user-password">Password</label>
			  <div class="col-md-4">
			    <input id="add-user-password" name="add-user-password" type="password" placeholder="password" class="form-control input-md" required>
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-md-4 control-label" for="add-user-roles-assigned">Assigned roles</label>
			  <div class="col-md-4">
			  	<?php foreach ($roles as $rid => $role): ?>
			  	  <?php 
			  	  	$role_default = ($role == 'default');
			  	  ?>
				  <div class="checkbox">
				    <label for="add-user-roles-assigned-<?=$rid?>">
				      <input type="checkbox" name="add-user-roles-assigned-<?=$rid?>" id="add-user-roles-assigned-<?=$rid?>" <?=($role_default ? 'checked disabled="disabled"' : '')?>>
				      <?=$role?>
				    </label>
				  </div>
			  	<?php endforeach ?>
			  </div>
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary submit">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- END: Add user functionality -->
<!-- User table -->
<table id="users" class="table table-striped">
	<thead>
		<th>Name</th>
		<th>Actions</th>
	</thead>
	<tbody>
		<?php foreach ($users as $uid => $username): ?>
			<?php include 'user.view.php'; ?>
		<?php endforeach ?>
	</tbody>
</table>