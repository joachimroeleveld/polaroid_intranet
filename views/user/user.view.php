<?php
	if (!empty($_POST)) {
		$uid = $_POST['uid'];
		$user_db = Database::get_manager()->select_quick(array('uid' => $uid), 'user');
		$username = $user_db[0]['username'];
	}
?>
<tr>
	<td class="name"><?=($username != 'admin' ? $username : '<em>admin (system user)</em>')?></td>
	<td class="actions">
		<button type="button" class="btn btn-default btn-xs edit-user" data-action="edit" data-toggle="modal" data-uid="<?=$uid?>" data-username="<?=$username?>">
			<span class="glyphicon glyphicon-pencil"></span>
			Edit
		</button>
		<?php if ($username != 'admin'): ?>
			<button type="button" class="btn btn-default btn-xs delete-user" data-action="delete" data-uid="<?=$uid?>">
				<span class="glyphicon glyphicon-remove"></span>
				Delete
			</button>
		<?php endif ?>
	</td>
</tr>