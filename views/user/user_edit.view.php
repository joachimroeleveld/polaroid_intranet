<?php
	require_once MODEL_PATH . 'UserManager.php';
	$USERS = new UserManager();
	$roles = $USERS->get_roles();
	$username = $_POST['username'];
	$uid = $_POST['uid'];
?>
<div class="modal fade edit-user-modal" id="edit-user-<?=$uid?>" tabindex="-1" role="dialog" aria-labelledby="edit-user-<?=$uid?>" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Edit user <em><?=$username?></em></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
			<div class="form-group">
			  <label class="col-md-4 control-label" for="edit-user-<?=$uid?>-username">Username</label>  
			  <div class="col-md-4">
			  	<input id="edit-user-<?=$uid?>-username" name="edit-user-<?=$uid?>-username" type="text" placeholder="username" class="form-control input-md" value="<?=$username?>" <?=($username == 'admin' ? 'disabled="disabled"' : '')?> required>
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-md-4 control-label" for="edit-user-<?=$uid?>-password">Password</label>
			  <div class="col-md-4">
			    <input id="edit-user-<?=$uid?>-password" name="edit-user-<?=$uid?>-password" type="password" placeholder="password" class="form-control input-md" required>
			    <small>If left empty, the current password will be used.</small>
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-md-4 control-label" for="edit-user-<?=$uid?>-roles-assigned">Assigned roles</label>
			  <div class="col-md-4">
			  	<?php foreach ($roles as $rid => $role): ?>
				  <div class="checkbox">
				    <label for="edit-user-<?=$uid?>-roles-assigned-<?=$rid?>">
				    	<?php 
							$user_role = Database::get_manager()->select_quick(array('uid' => $uid, 'rid' => $rid), 'user_role');
			  	  			$role_default = ($role == 'default');
							$checked = (isset($user_role) || $role_default ? 'checked' : '');
							$disabled = ($role_default || $username == 'admin' ? ' disabled="disabled"' : '');
						?>
				      	<input type="checkbox" name="edit-user-<?=$uid?>-roles-assigned-<?=$rid?>" id="edit-user-<?=$uid?>-roles-assigned-<?=$rid?>" <?=$checked . $disabled?>>
				      <?=$role?>
				    </label>
				  </div>
			  	<?php endforeach ?>
			  </div>
			</div>	
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary submit" data-uid="<?=$uid?>">Save changes</button>
      </div>
    </div>
  </div>
</div>