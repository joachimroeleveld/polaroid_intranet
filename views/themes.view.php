<?php
	global $THEMES;
	$themes = $THEMES->get_indexed_themes(); 
?>
<table id="themes" class="table table-striped">
	<thead>
		<th>Name</th>
		<th>Enabled</th>
	</thead>
	<tbody>
		<?php foreach ($themes as $theme): ?>
		  <tr>
		  	<td class="name"><?=$theme?></td>
		  	<td class="state">
		  		<form role="form">
					<div class="radio">
					    <label>
					    	<?php
					    		$db_theme = Database::get_manager()->select_quick(array('name' => $theme), 'theme');
					    		if(isset($db_theme))
					    			$enabled = $db_theme[0]['status'];
					    		else
					    			$enabled = 0;
				    		?>
				      		<input class="state-check" type="radio" <?php echo ($enabled == 1 ? 'checked="checked"' : '') ?>>
					    </label>
				  	</div>
			  	</form>
		  	</td>
		  </tr>
		<?php endforeach ?>
	</tbody>
</table>