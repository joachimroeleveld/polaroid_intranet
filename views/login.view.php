<form class="form-horizontal" name="login">
  <fieldset>
    <legend></legend>
    <div class="form-group">
      <label class="col-md-4 control-label" for="username">Username</label>  
      <div class="col-md-4">
        <input type="text" id="username" name="username" class="form-control input-md" placeholder="username" required>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-4 control-label" for="password">Password</label>
      <div class="col-md-4">
        <input type="password" id="password" name="password" class="form-control input-md" placeholder="password" required>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-4 control-label" for="submit"></label>
      <div class="col-md-4">
        <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Login">
        <a class="help" href="mailto:<?=$GLOBALS['config']['email_admin']?>?Subject=<?=$GLOBALS['config']['brand_name']?>%20Intranet">Need help?</a>
      </div>
    </div>
  </fieldset>
</form>
