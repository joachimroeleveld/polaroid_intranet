<nav id="topBar" class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
  	<div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#topBarCollapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?=(User::is_logged_in() ? BASE_URL . 'front' : BASE_URL . 'user/login')?>">
        <span class="polaroid-logo"></span><span class="text"> <?=$GLOBALS['config']['brand_name']?></span>
      </a>
    </div>
  	<div class="collapse navbar-collapse" id="topBarCollapse">  
  		<?php
        global $THEMES;
        global $MENU;

        $args = array(
          'tree' => $MENU->get_user_tree(),
          'max_depth' => 3, 
          'length' => 10
        );
        print $THEMES->theme_function_invoke('menu_generate', $args);
      ?>
	</div>
  </div>
</nav>
