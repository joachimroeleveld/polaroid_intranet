<?php
  $rolename = $_POST['rolename'];
  $rid = $_POST['rid'];
?>
<div class="modal fade edit-role-modal" id="edit-role-<?=$rid?>" tabindex="-1" role="dialog" aria-labelledby="edit-role-<?=$rid?>" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Edit role <em><?=$rolename?></em></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
			<div class="form-group">
			  <label class="col-md-4 control-label" for="edit-role-<?=$rid?>-name">Name</label>  
			  <div class="col-md-4">
			  	<input id="edit-role-<?=$rid?>-name" name="edit-role-<?=$rid?>-name" type="text" placeholder="name" class="form-control input-md" value="<?=$rolename?>" required>
			  </div>
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary submit" data-rid="<?=$rid?>">Save changes</button>
      </div>
    </div>
  </div>
</div>