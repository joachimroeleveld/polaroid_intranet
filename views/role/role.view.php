<?php
	if (!empty($_POST)) {
		$rid = $_POST['rid'];
		$role_db = Database::get_manager()->select_quick(array('rid' => $rid), 'role');
		$role_name = $role_db[0]['name'];
	}
?>
<tr>
	<td class="name"><?=$role_name?></td>
	<td class="actions">
		<button type="button" class="btn btn-default btn-xs edit-role" data-action="edit" data-toggle="modal" data-target="#edit-role-<?=$rid?>" data-rid="<?=$rid?>" data-role-name="<?=$role_name?>">
			<span class="glyphicon glyphicon-pencil"></span> 
			Edit
		</button>
		<button type="button" class="btn btn-default btn-xs delete-role" data-action="delete" data-rid="<?=$rid?>">
			<span class="glyphicon glyphicon-remove"></span>
			Delete
		</button>
	</td>
</tr>