<?php
	global $USERS;
	$roles = $USERS->get_roles();
	$system_roles = $USERS->get_system_roles();
	// Remove the system roles from the editable role list
	foreach ($system_roles as $role) {
		unset($roles[array_search($role, $roles)]);
	}
?>
<!-- Add role functionality -->
<button type="button" class="btn btn-default add-role" data-action="add" data-toggle="modal" data-target="#add-role-modal">
	<span class="glyphicon glyphicon-plus"></span> 
	Add role
</button>
<div class="modal fade" id="add-role-modal" tabindex="-1" role="dialog" aria-labelledby="add-role-modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Add role</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
			<div class="form-group">
			  <label class="col-md-4 control-label" for="add-role-name">Name</label>  
			  <div class="col-md-4">
			  	<input id="add-role-name" name="add-role-name" type="text" placeholder="name" class="form-control input-md" required>
			  </div>
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary submit">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- END: Add role functionality -->
<!-- Role table -->
<table id="roles" class="table table-striped">
	<thead>
		<th>Name</th>
		<th>Actions</th>
	</thead>
	<tbody>
		<!-- System roles -->
		<?php foreach ($system_roles as $role): ?>
			<tr>
				<td><em><?=$role?></em></td>
				<td><em>system role</em></td>
			</tr>
		<?php endforeach ?>
		<!-- END: System roles -->
		<!-- User defined roles -->
		<?php foreach ($roles as $rid => $role_name): ?>
			<?php include 'role.view.php'; ?>
		<?php endforeach ?>
		<!-- END: User defined roles -->
	</tbody>
</table>
<!-- END: Role table -->