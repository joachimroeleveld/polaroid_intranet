<ul class="nav nav-tabs">
  <li class="active"><a href="#list" data-toggle="tab">List</a></li>
  <li><a href="#uninstall" data-toggle="tab">Uninstall</a></li>
</ul>
<div class="tab-content">
  <div class="tab-pane active" id="list">
	<?php $plugins = PluginManager::get_plugins(); ?>
	<?php if (isset($plugins)): ?>
		<table id="plugins" class="table table-striped">
			<thead>
				<th>Name</th>
				<th>Enabled</th>
			</thead>
			<tbody>
				<?php foreach ($plugins as $plugin): ?>
				  <tr>
				  	<td class="name"><?=$plugin?></td>
				  	<td class="state">
				  		<form role="form">
							<div class="checkbox">
							    <label>
							    	<?php
							    		$db_plugin = Database::get_manager()->select_quick(array('name' => $plugin), 'plugin');
							    		if(isset($db_plugin))
							    			$enabled = $db_plugin[0]['status'];
							    		else
							    			$enabled = 0;
						    		?>
						      		<input class="state-check" type="checkbox" <?php echo ($enabled == 1 ? 'checked="checked"' : '') ?>>
							    </label>
						  	</div>
					  	</form>
				  	</td>
				  </tr>
				<?php endforeach ?>
			</tbody>
		</table>
	<?php else: ?>
		<p>There are no plugins available.</p>	
	<?php endif ?>
  </div>
  <div class="tab-pane" id="uninstall">
	<?php $uninstallable_plugins = PluginManager::get_uninstallable_plugins(); ?>
	<?php if (isset($uninstallable_plugins)): ?>
		<table id="plugins_uninstall" class="table table-striped">
			<thead>
				<th>Name</th>
				<th>Uninstall</th>
			</thead>
			<tbody>
				<?php foreach ($uninstallable_plugins as $plugin): ?>
				  <tr>
				  	<td class="name"><?=$plugin?></td>
				  	<td class="action">
				  		<button type="button" class="btn btn-primary btn-xs uninstall-plugin">Uninstall</button>
				  	</td>
				  </tr>
				<?php endforeach ?>
			</tbody>
		</table>
	<?php else: ?>
		<p>There are no plugins that can be uninstalled.</p>	
	<?php endif ?>
  </div>
</div>