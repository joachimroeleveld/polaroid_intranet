<?php
	// Since this file is directly invoked with AJAX,
	// the site is not bootstrapped, so the constants
	// have to be included manually. In order for the 
	// constants to work, the SITE_ROOT constant is
	// defined from here.
	defined('SITE_ROOT') or
		define('SITE_ROOT', dirname(__FILE__) . '/../');
	require_once 'models/includes/constants.inc';
	require_once SITE_ROOT . 'settings.php';
	require_once 'models/Database.php';

	if(isset($_POST['function'])){
		if (!isset($_POST['data'])) {
			$data = '';
		} else {
			$data = $_POST['data'];
		}

	    call_user_func($_POST['function'], $data);
	}

	function successful_return($messages = array(), $warnings = array(), $data = array()) {
		$return = array();

		foreach ((array)$messages as $message) {
			$return['messages'][] = $message;
		}
		foreach ((array)$warnings as $warning) {
			$return['warnings'][] = $warning;
		}
		foreach ((array)$data as $key => $value) {
			$return['data'][$key] = $value;
		}

		echo json_encode($return);
	}

	function error_return($error) {
		echo json_encode(array('error' => $error));
		die();
	}

	/**
	 * Creates an output buffer from a file's 
	 * output and returns it.
	 * @param $data array. Data that should
	 * 	be available (trough POST) to the file.
	 * @param $file string. The file to buffer.
	 */
	function html_return($data, $file) {
		$_POST = $data;

		ob_start();
		include $file;
		$buffer = ob_get_clean();

		successful_return(null, null, array('buffer' => $buffer));
	}

	/**
	 * Lets a system view generate it's HTML
	 * and returns it.
	 * @param $data array. Data which is passed
	 * 	to the view.
	 */
	function system_view_return($data) {
		html_return($data['data'], VIEW_PATH . $data['view'] . '.view.php');
	}

	/**
	 * Parses serialized form data (to an array).
	 * @param $serialized string. The serialized 
	 * 	form data.
	 */
	function parse_serialized($serialized) {
		$data = array();
		parse_str($serialized, $data);

		return $data;
	}

	function save_plugin($data) {
		require_once 'models/PluginManager.php';
		$manager = new PluginManager();

		if($data['state'] == 1) {
			$manager->enable_plugin($data['plugin']);
		} else {
			$manager->disable_plugin($data['plugin']);
		}

	 	successful_return('Plugin ' . $data['plugin'] . (($data['state'] == 1) ? ' enabled' : ' disabled'));
	}

	function uninstall_plugin($data) {
		require_once 'models/PluginManager.php';
		$installer = new PluginManager();
		$installer->uninstall_plugin($data['plugin']);

		successful_return('Plugin ' . $data['plugin'] . ' uninstalled.');
	}

	function save_theme($data) {
		require_once 'models/ThemeManager.php';
		$manager = new ThemeManager();
		$manager->switch_enabled_theme($data['theme']);

	 	successful_return('Theme ' . $data['theme'] . ' enabled.');
	}

	function save_permission($data) {
		if($data['state'] == 1) {
			Database::get_manager()->insert(
				array(
					'rid' => $data['rid'], 
					'pid' => $data['pid'],
				),
				'role_permission'
			);
		} else {
			Database::get_manager()->delete_quick(
				array(
					'rid' => $data['rid'], 
					'pid' => $data['pid'],
				),
				'role_permission'
			);
		}

		successful_return('Permission successfully changed.');
	}

	/**
	 * (helper function) Extracts role id's from the 
	 * checkboxes from the form data of add/edit forms.
	 * @param $data array. Parsed form data.
	 * @param $prefix string. The string with which
	 * 	the name attribute of a role checkbox begins.
	 */
	function extract_roles($data, $prefix) {
		$rids = null;
		// Get assigned role id's
		foreach ($data as $key => $value) {
			// If the key (value's name attribute) 
			// begins with the role checkbox prefix
			if(strpos($key, $prefix) === 0) {
				// Substract the role id from the key
				$rids[]	= substr($key, strlen($prefix));
			}
		}

		return $rids;
	}

	function add_role($data) {
		require_once MODEL_PATH . 'UserManager.php';
		$users = new UserManager();
		$parsed_data = parse_serialized($data['data']);

		if ($users->add_role($parsed_data['add-role-name'])) {
			$rid = array_search($parsed_data['add-role-name'], $users->get_roles());
			successful_return($users->message, array(), array('rid' => $rid));
		} else {
			error_return($users->error);
		}
	}	

	function delete_role($data) {
		require_once MODEL_PATH . 'UserManager.php';
		$users = new UserManager();

		if ($users->delete_role($data['rid'])) {
			successful_return($users->message);
		} else {
			error_return($users->error);
		}
	}

	function edit_role($data) {
		require_once MODEL_PATH . 'UserManager.php';
		$users = new UserManager();
		$parsed_data = parse_serialized($data['data']);

		if ($users->edit_role($data['rid'], $parsed_data['edit-role-' . $data['rid'] . '-name'])) {
			successful_return($users->message);
		} else {
			error_return($users->error);
		}
	}

	function add_user($data) {
		require_once MODEL_PATH . 'UserManager.php';
		$users = new UserManager();
		$parsed_data = parse_serialized($data['data']);

		if ($users->register_user($parsed_data['add-user-username'], $parsed_data['add-user-password'], extract_roles($parsed_data, 'add-user-roles-assigned-'))) {
			$uid = array_search($parsed_data['add-user-username'], $users->get_users());
			successful_return($users->message, array(), array('uid' => $uid));
		} else {
			error_return($users->error);
		}
	}

	function delete_user($data) {
		require_once MODEL_PATH . 'UserManager.php';
		$users = new UserManager();

		if ($users->delete_user($data['uid'])) {
			successful_return($users->message);
		} else {
			error_return($users->error);
		}
	}

	function edit_user($data) {
		require_once MODEL_PATH . 'UserManager.php';
		$users = new UserManager();
		$parsed_data = parse_serialized($data['data']);
		$password = $parsed_data['edit-user-' . $data['uid'] . '-password'];
		$roles = extract_roles($parsed_data, 'edit-user-' . $data['uid'] . '-roles-assigned-');

        // If the user that is edited is the admin user, make sure the fields are filled.
		$admin_uid = array_search('admin', $users->get_users());
        if ($data['uid'] != $admin_uid) {
			$username = $parsed_data['edit-user-' . $data['uid'] . '-username'];
        } else {
            $username = 'admin';
            $roles[] = array_search('administrator', $users->get_roles());
        }

		// If password is not given, set it to it's current value
		if (empty($password)) {
			$user_db = Database::get_manager()->select_quick(array('uid' => $data['uid']), 'user');
			$password = $user_db[0]['password'];
		}

		if ($users->edit_user($data['uid'], $username, $password, $roles)) {
			successful_return($users->message);
		} else {
			error_return($users->error);
		}
	}

	function login($data) {
		require_once MODEL_PATH . 'User.php';

		$user = new User();
		$data = parse_serialized($data['data']);
		
		if ($user->login($data['username'], $data['password'])) {
			successful_return($user->message);
		} else {
			error_return($user->error);
		}
	}

