<?php
	require_once 'theme_functions/menu.inc';

	class Menu
	{
		/**
		 * When this tree is built using build_system_menu,
		 * this tree contains any link defined in the system, 
		 * regardless of any permissions. Useful for checking
		 * if a path exists.
		 */
		private $system_tree;
		/**
		 * When this tree is built using build_user_menu,
		 * this tree contains any link the current visitor,
		 * has access to (based on his permissions). Useful
		 * for output to the screen.
		 */
		private $user_tree;

		function __construct() {
			$this->system_tree = array();
			$this->user_tree = array();
		}

		/**
		 * Recursively sorts a menu tree based on its weights
		 * @param tree array. The menu tree to be sorted.
		 */
		private static function sort_tree($tree) {
			if (count($tree) > 1) {
				$key_weight = array();
				$not_weighted = array();

				// Create an array which keys and corresponding weights.
				// We store the keys which don't have a weight assigned 
				// in another array with weight 0.
				foreach ($tree as $key => $value) {
					if(isset($value['options']['weight']))
						$key_weight[$key] = $value['options']['weight'];
					else
						$not_weighted[$key] = 0;
				}

				// Sort the array with keys and weights the on the values, 
				// so we have an array of keys sorted by weights, which 
				// is what we want. 
				asort($key_weight);
				// Merge the array sorted by weights with the zero-weighted
				// array so their order is not changed in the sort above.
				$key_weight = array_merge($key_weight, $not_weighted);

				// Recreate a tree with the items in sorted order, with
				// each item's subitems recursively sorted.
				foreach ($key_weight as $key => $value) {
					// Make sure an options array is excluded.
					if ($key != 'options')
						$sorted_menu[$key] = self::sort_tree($tree[$key]);
				}

				// Append the options, since we skipped them in the 
				// previous step.
				if (isset($tree['options']))
					$sorted_menu['options'] = $tree['options'];

				return $sorted_menu;
			} else {
				return $tree;
			}
		}

		/**
		 * Recursive helper function for build_menu(). Recursively 
		 * generates the appropriate menu structure for one path.
		 * Options of each path are stored under the options array.
		 * @param $pieces array. The by forward slashed divided 
		 *  pieces of the path. E.g. the array of 
		 *  'http://test.com/test1/test2' would be: 
		 *  array('test1', 'test2')
		 * @param $tree array(). The menu tree to add the item to.
		 * @param $options array(). The options for the menu item.
 		 * @param $check_permissions boolean. When this is set to
		 * true, the link is built based on the permissions of the
		 * current visitor. If the visitor doesn't have permission
		 * to access the link, this function will return null.
		 */
		private static function build_menu_item($pieces, $tree, $options, $check_permissions = false) {
			if ($check_permissions)
				require_once 'path_handler.php';
			$piece = $pieces[0];

			// If desired, check the permissions here
			if (!$check_permissions || ($check_permissions && path_check_user_access(null, $options))) {
				if (count($pieces) > 1) {
					// In the next iteration we want to get a level 
					// deeper
					unset($pieces[0]);
					// Reset the array keys
					$pieces = array_values($pieces);

					if(!isset($tree[$piece])) {
						$tree[$piece] = array();
					}

					// Recursively get the subtree
					$subtree = self::build_menu_item($pieces, $tree[$piece], $options, $check_permissions);
					if (isset($subtree))
						$tree[$piece] = $subtree;
					else
						return null;
				} else {
					// Set the options for this leaf. This is the 
					// last upward step in an iteration.
					$tree[$piece]['options'] = $options;
				}

				return $tree;
			}

			return null;
		}

		/**
		 * Collects HOOK_menu from all plugins and fills the menu 
		 * with all the paths defined in these plugins.
		 * @param $check_permissions boolean. When this is set to
		 * true, the menu will be built based on the permissions
		 * of the current visitor. If the visitor doesn't have 
		 * permission to access a link, the link is then removed
		 * from the tree.
		 */
		public function build_menu($check_permissions = false) {
			global $PLUGINS;
			$tree = array();

			// For each enabled plugin
			foreach ($PLUGINS->get_active_plugins() as $plugin) {
				// Get HOOK_menu
				$menu_items = PluginManager::plugin_invoke($plugin, 'menu');

				foreach ($menu_items as $url => $options) {
			       	$pieces = explode('/', $url);
			       	// Since 'options' is reserved, make sure a 
			       	// link containing this word is not added to 
			       	// the menu tree.
			       	if(in_array('options', $pieces)) {
			       		break;
			       	}
			       	// Generate a tree for this path
			       	$path_tree = self::build_menu_item($pieces, $tree, $options, $check_permissions);
			       	if (isset($path_tree))
			       		// Merge the tree with the existing menu tree
			       		$tree = array_merge($tree, $path_tree);
				}
			}

			// Unset a parent if it has no subitems. This happens
			// when a visitor has no access to any of the
			// parent's subitems.
			if ($check_permissions) {
				foreach ($tree as $url => $subitems) {
					if (count($subitems) == 1 && !isset($subitems['options']['page arguments']))
						unset($tree[$url]);
				}
			}

			// Return the tree (sorted)
			return self::sort_tree($tree);
		}

		/**
		 * Builds the system menu tree
		 */
		public function build_system_menu() {
			$this->system_tree = $this->build_menu();
		}

		/**
		 * Builds the user menu tree
		 */
		public function build_user_menu() {
			$this->user_tree = $this->build_menu(true);
		}

		/**
		 * Returns the options for a particular menu item.
		 * @param $path string. The path to get the options from.
		 */
		public function get_path_options($path) {
			$pieces = explode('/', $path);

			return self::get_path_options_helper($pieces, $this->system_tree);
		}

		/**
		 * Recursive helper for get_path_options(). Walks down
		 * in the menu tree and returns the options for the 
		 * specific menu item.
 		 * @param $pieces array. The by forward slashed divided 
		 *  pieces of the path. E.g. the array of 
		 *  'http://test.com/test1/test2' would be: 
		 *  array('test1', 'test2')
		 * @param $tree array(). The menu tree containing the
		 *  item to get the options from.
		 */
		private static function get_path_options_helper($pieces, $tree) {
			$piece = $pieces[0];

			// Does the path until now exists?
			if (array_key_exists($piece, $tree)) {
				if (count($pieces) > 1) {
					// In the next iteration we want to get a 
					// level deeper
					unset($pieces[0]);
					// Reset the array keys
					$pieces = array_values($pieces);

					// Walk recursively trough the children
					return self::get_path_options_helper($pieces, $tree[$piece]);
				} else {
					$item = $tree[$piece];

					if(isset($item['options'])) {
						return $item['options'];
					}
				}
			}
		}

		/**
		 * Generates attributes for an html element.
		 * @param $attributes array. The attributes to generate
		 * 	the html for.
		 */
		public static function html_attributes($attributes) {
			$html_attributes = '';
			foreach ($attributes as $attribute => $values) {
				// If multiple values, convert them to one string, separated by spaces
				if (is_array($values)) {
					$html_values = '';
					foreach ($values as $value) {
						$html_values .= $value . ' ';
					}
					$html_values = trim($html_values);
				} else {
					$html_values = $values;
				}
				
				$html_attributes .= $attribute . '="' . $html_values . '" ';
			}

			return trim($html_attributes);
		}

		/**
		 * Checks if a path exists.
		 * @param $path string. The path to check.
		 */
		public function path_exists($path) {
			$result = $this->get_path_options($path);

			return isset($result);
		}

		public function get_system_tree() {
			return $this->system_tree;
		}

		public function get_user_tree() {
			return $this->user_tree;
		}
	}



