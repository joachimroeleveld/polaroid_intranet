<?php
	require_once 'Templater.php';

	class Page {
		private $templater;
		private $scripts = array();
		private $stylesheets = array();
		public $bodyClasses = array();

		public function __construct() {
			$this->templater = new Templater("views/main.view.php");
		}

		private function add_theme_stylesheets($stylesheets, $theme) {
			foreach ($stylesheets as $stylesheet) {
				$this->stylesheets[] = THEME_URL . $theme . '/' . $stylesheet;
			}
		}

		private function add_theme_scripts($scripts, $theme) {
			foreach ($scripts as $script) {
				$this->scripts[] = THEME_URL . $theme . '/' . $script;
			}
		}

		/**
		 * Retrieve additional stylesheets and scripts from  
		 * a theme (declared in it's .theme file) and add
		 * them to the corresponding arrays.
		 * @param $theme string. The theme to get the sources
		 * 	from.
		 */
		private function add_theme_libraries($theme) {
			include (THEME_PATH . $theme . '/' . $theme . '.theme');
			$theme_config_array = ${'theme_' . $theme};

			if (isset($theme_config_array['stylesheets']))
				$this->add_theme_stylesheets($theme_config_array['stylesheets'], $theme);
			if (isset($theme_config_array['scripts']))
				$this->add_theme_scripts($theme_config_array['scripts'], $theme);
		}

		private function add_plugin_stylesheets($stylesheets, $plugin) {
			foreach ($stylesheets as $stylesheet) {
				$this->stylesheets[] = PLUGIN_URL . $plugin . '/' . $stylesheet;
			}
		}

		private function add_plugin_scripts($scripts, $plugin) {
			foreach ($scripts as $script) {
				$this->scripts[] = PLUGIN_URL . $plugin . '/' . $script;
			}
		}

		/**
		 * Retrieve additional stylesheets and scripts from  
		 * a plugin (declared in it's .plugin file) and add
		 * them to the corresponding arrays.
		 * @param $plugins array. An optional array with 
		 * 	plugins from which the libraries should be 
	 	 * 	added. If this is provided, no other plugin
	 	 * 	libraries will be added.
		 */
		private function add_plugin_libraries($plugins = null) {
			global $PLUGINS;

			if (!isset($plugins)) {
				// For each enabled plugin
				foreach ($PLUGINS->get_active_plugins() as $plugin) {
					if ($plugin != 'system') {
						$this->add_plugin_libraries_helper($plugin);
					}
				}
			} else {
				// For each plugin in the argument array
				foreach ($plugins as $plugin) {
					$this->add_plugin_libraries_helper($plugin);
				}
			}
		}

		private function add_plugin_libraries_helper($plugin) {
			// Get HOOK_libraries
			$libraries = PluginManager::plugin_invoke($plugin, 'libraries');

			if (isset($libraries['stylesheets']))
				$this->add_plugin_stylesheets($libraries['stylesheets'], $plugin);
			if (isset($libraries['scripts']))
				$this->add_plugin_scripts($libraries['scripts'], $plugin);
		}

		public function set_body($body) {
			$this->templater->set("body", $body->parse());
		}

		public function set_title($title) {
			$this->templater->title = $title;
		}

		function publish() {
			global $THEMES;
			global $CONTENT;

			// Set the body classes
			$this->templater->set('bodyClass', implode(' ', $this->bodyClasses));

			// Add system libraries
			$this->add_plugin_libraries(array('system'));

			// If the page is from a plugin view, we want to include 
			// only the libraries from this plugin.
			if(isset($CONTENT->plugin_view))
				$this->add_plugin_libraries(array($CONTENT->plugin_view));

			// Add stylesheets and scripts from the enabled theme.
			$enabled_theme = $THEMES->get_enabled_theme();
			$this->add_theme_libraries($enabled_theme);

			$stylesheets = '';
			foreach ($this->stylesheets as $stylesheet) {
				$stylesheets .= '@import "' . $stylesheet . '";';
			}
			$this->templater->stylesheets = $stylesheets;

			$scripts = '';
			foreach ($this->scripts as $script) {
				$scripts .= '<script type="text/javascript" src="' . $script . '"></script>';
			}
			$this->templater->scripts = $scripts;

			$this->templater->publish();
		}
	}



