<?php
	class PluginInstaller
	{
		private $plugin;
		private $manager;
		private $install_file;

		function __construct($plugin, $manager) {
			$this->plugin = $plugin;
			$this->manager = $manager;
			$plugin_path = $this->manager->get_plugin_path($plugin);
			$this->install_file = $plugin_path . $plugin . '.install';
			require_once $this->install_file;
		}

		/**
		 * Get the schema from HOOK_schema of a plugin's install file.
		 * @param $plugin string. The plugin to get the install scheme from.
		 * @return array. The schema
		 * @return false. When HOOK_schema is not defined.
		 */
		public static function get_schema($plugin) {
			$install_file = PluginManager::get_plugin_path($plugin) . $plugin . '.install';

			if(is_file($install_file)) {
				require_once $install_file;
				return PluginManager::plugin_invoke($plugin, 'schema');
			}
		}

		/**
		 * Returns the tables from a schema which exist in the database.
		 * @param $schema array. The schema.
		 * @return array. An array with the tables that exist
		 * @return false. When no table exists
		 */
		public static function get_schema_remains($schema) {
			if (isset($schema)) {
				foreach ($schema as $name => $table) {
					global $config;
					$schemas = Database::get_manager()->select(
						"SHOW TABLES FROM " . $config['db']['main']['dbname'] . " LIKE 'plugin'");
					if(isset($schemas[0]))
						$tables[] = $name;
				}

				if (!empty($tables))
					// In order to avoid foreign key constraint conflicts,
					// we reverse the array.
					return array_reverse($tables);
			}
		}

		public function install() {
			$this->_schema_install($this->manager->plugin_invoke($this->plugin, 'schema'));
			$this->_permissions_install($this->manager->plugin_invoke($this->plugin, 'permissions'));
		}

		public function uninstall() {
			$this->_schema_uninstall($this->get_schema_remains($this->get_schema($this->plugin)));
			$this->_permissions_uninstall($this->manager->plugin_invoke($this->plugin, 'permissions'));

			// Delete the plugin from the plugin table
			Database::get_manager()->delete_quick(array('name' => $this->plugin), 'plugin');

		}

		private function _schema_uninstall($schema) {
			if (isset($schema)) {
				// Delete all tables
				foreach ($schema as $table) {
					Database::get_manager()->generic('DROP TABLE ' . $table);
				}
			}
		}

		private function _schema_install($schema) {
			if (isset($schema)) {
				require_once 'includes/ddl_mysql.inc';

				// Let the database create each table defined in the .install file
				foreach ($schema as $name => $table) {
					$sql = create_table_sql($name, $table);
					Database::get_manager()->generic($sql);
				}
			}
		}

		private function _permissions_install($permissions) {
			if (isset($permissions)) {
				// Insert permissions defined in the .install file
				foreach ($permissions as $name => $permission) {
					$db_permission = Database::get_manager()->select_quick(array('name' => $name), 'permission', 1);
					if (!isset($db_permission)) {
						$permission['name'] = $name;
						$permission['plugin'] = $this->plugin;
						
						Database::get_manager()->insert($permission, 'permission');
					}
				}
			}
		}

		private function _permissions_uninstall($permissions) {
			if (isset($permissions)) {
				// Delete permissions
				$sql_p = 'DELETE FROM permission WHERE plugin = ?';
				Database::get_manager()->generic($sql_p, array($this->plugin));
			}
		}

	}