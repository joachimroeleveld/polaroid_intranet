<?php
    /**
     * NOTE: in this class, password_* functions from PHP 5.5
     * are used. When using a PHP version older than 5.5, you
     * have to include password.inc from the includes folder:
     * require_once 'includes/password.inc';
     */
	class UserManager 
	{
        private $role_default_rid;
        private $system_roles = array('administrator', 'default');
		private $users = array();
		private $roles = array();
        public $error = array();
        public $message = array();

		function __construct() {
            $admin_user_db = Database::get_manager()->select_quick(array('username' => 'admin'), 'user')[0];
            $admin_exists = isset($admin_user_db);
            if (!$admin_exists) {
                // Create system roles
                foreach ($this->system_roles as $role) {
                    Database::get_manager()->insert(array('name' => $role), 'role');
                }
            }

            // Get and fill 'bean data'
            $users = Database::get_manager()->select('SELECT * FROM user');
            $roles = Database::get_manager()->select('SELECT * FROM role');
            if (isset($roles)) {
                foreach ($roles as $role) {
                    $this->roles[$role['rid']] = $role['name'];
                }
            }
            if (isset($users)) {
                foreach ($users as $user) {
                    $this->users[$user['uid']] = $user['username'];
                }
            }

            if (!$admin_exists) {
                // Create user with administrator privilleges
                $this->register_user('admin', 'password', array(array_search('administrator', $this->roles)));
            }
            $this->role_default_rid = array_search('default', $this->roles);
		}

        public function add_role($role_name) {
            if ($this->validate_role($role_name)) {
                Database::get_manager()->insert(array('name' => $role_name), 'role');

                // Get the role id for the just created role
                $new_role = Database::get_manager()->select_quick(array('name' => $role_name), 'role', 1);
                $rid = $new_role[0]['rid'];

                $this->roles[$rid] = $role_name;
                $this->message = "Role created successfully.";
                return true;
            }

            return false;
        }

        public function delete_role($rid) {
            $query = Database::get_manager()->delete_quick(array('rid' => $rid), 'role');

            if (!isset($query)) {
                unset($this->role[$rid]);
                $this->message = "Deleted role successfully.";
                return true;
            } else {
                $this->error = "Could not delete role.";
                return false;
            }
        }

        public function edit_role($rid, $role_name) {
            if ($this->validate_role($role_name, $rid)) {
                Database::get_manager()->update_quick(array('name' => $role_name), array('rid' => $rid), 'role');

                $this->roles[$rid] = $role_name;
                $this->message = "The role has been edited successfully.";
                return true;
            }

            return false;
        }

        public function register_user($username, $password, $roles = array()) {
            // Validate input
            if ($this->validate_user($username, $password)) {
                // Crypt the user's password with PHP 5.5's password_hash() function.
                // Results in a 60 character hash string.
                $password_hash = password_hash($password, PASSWORD_DEFAULT);

                // Insert the user into the database
                $row = array(
                    'username' => $username,
                    'password' => $password_hash,
                );
                Database::get_manager()->insert($row, 'user');

                // Get the user id for the just created user
                $new_user = Database::get_manager()->select_quick(array('username' => $username), 'user', 1);
                $uid = $new_user[0]['uid'];

                // A user always is always assigned the 'default' role
                $roles[] = $this->role_default_rid;

                // Assign roles to the user
                foreach ($roles as $rid) {
                    Database::get_manager()->insert(array(
                        'uid' => $uid,
                        'rid' => $rid,
                    ), 'user_role');
                }

            	$this->users[$uid] = $username;
                $this->message = "The user has been created successfully.";

                return true;
            }
            
            return false;
        }

        public function edit_user($uid, $username, $password, $roles = array()) {
            // Validate input
            if ($this->validate_user($username, $password, $uid)) {
                // Crypt the user's password with PHP 5.5's password_hash() function.
                // Results in a 60 character hash string.
                $password_hash = password_hash($password, PASSWORD_DEFAULT);

                // Update user database row
                $new_values = array(
                    'username' => $username,
                    'password' => $password_hash,
                );
                Database::get_manager()->update_quick($new_values, array('uid' => $uid), 'user');

                // Delete old roles
                Database::get_manager()->delete_quick(array('uid' => $uid), 'user_role');

                // A user always is always assigned the 'default' role
                $roles[] = $this->role_default_rid;

                // Assign new roles to the user
                foreach ($roles as $rid) {
                    Database::get_manager()->insert(array(
                        'uid' => $uid,
                        'rid' => $rid,
                    ), 'user_role');
                }

                $this->users[$uid] = $username;
                $this->message = "The user has been edited successfully.";

                return true;
            }

            return false;
        }

        public function delete_user($uid) {
            $query = Database::get_manager()->delete_quick(array('uid' => $uid), 'user');

            if (!isset($query)) {
                unset($this->users[$uid]);
                $this->message = "Deleted user successfully.";
                return true;
            } else {
                $this->error = "Could not delete user.";
                return false;
            }
        }

        /**
         * Validates user form input.
         * @param $uid int. When the username is checked for existence,
         *  this uid will be ignored (useful when editing a user which
         *  on submit has (of course) the same name).
         */
        private function validate_user($username, $password, $uid = null) {
            // Check if user already exists
            $already_exists = false;
            if($existing_uid = array_search($username, $this->users)) {
                if (!isset($uid) || (isset($uid) && $existing_uid != $uid)) {
                    $already_exists = true;   
                }
            }

            if (empty($username)) {
                $this->error = "Empty username";
                return false;
            } elseif (empty($password)) {
                $this->error = "Empty password";
                return false;
            } elseif ($already_exists) {
                $this->error = "Username is already taken.";
                return false;
            } elseif (strlen($password) < 6) {
                $this->error = "Password has a minimum length of 6 characters";
                return false;
            } elseif (strlen($username) > 64 || strlen($username) < 2) {
                $this->error = "Username cannot be shorter than 2 or longer than 64 characters";
                return false;
            } elseif (!preg_match('/^[a-z\d]{2,64}$/i', $username)) {
                $this->error = "Username does not fit the name scheme: only a-Z and numbers are allowed, 2 to 64 characters";
                return false;
            } elseif (!empty($username)
                && strlen($username) <= 64
                && strlen($username) >= 2
                && preg_match('/^[a-z\d]{2,64}$/i', $username)
                && !empty($password)) 
            {
                return true;
            } else {
                $this->error = "An unknown error occurred.";
                return false;
            }
        }

        /**
         * Validates role form input.
         * @param $rid int. When the rolename is checked for existence,
         *  this rid will be ignored (useful when editing a role which
         *  on submit has (of course) the same name).
         */
        private function validate_role($rolename, $rid = null) {
            // Check if role already exists
            if($existing_rid = array_search($rolename, $this->roles)) {
                if (!isset($rid) || (isset($rid) && $existing_rid != $rid)) {
                    $this->error = "Role already exists.";
                    return false;
                }
            }

            return true;
        }

		public function get_users() {
			return $this->users;
		}

		public function get_roles() {
			return $this->roles;
		}

        public function get_system_roles() {
            return $this->system_roles;
        }
	}