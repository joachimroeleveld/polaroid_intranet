<?php
	/**
	 * Generates an HTML menu (<ul>, <li> structure).
 	 * @param $tree array. The menu tree object.
	 * @param $max_depth int. The max amount of levels 
	 *  of the tree that will be generated.
	 * @param $length int. The max amount of top tree 
	 *  items that will be generated.
	 */
	function theme_menu_generate(&$variables) {
		global $MENU;
		$length_counter = 0;

		$active_item = explode('/', get_path());

		$output = '<ul class="menu">';
		foreach ($variables['tree'] as $key => $item) {
			$attributes = array();
			if ($length_counter < $variables['length']) {
				$length_counter++;

				if (isset($item['options']['attributes'])) {
	 				$attributes = $item['options']['attributes'];
	 				// Make sure attribute value(s) is/are always an array
					foreach ($attributes as $attribute => $values) {
						if (is_string($values))
							$attributes[$attribute] = array($values);
					}
				}
				if ($active_item[0] == $key)
					$attributes['class'][] = 'active';

				$output .= '<li ' . (isset($attributes) ? Menu::html_attributes($attributes) : '') . '>' . 
								// Print href only when a page callback is defined
								'<a href="' . (isset($item['options']['page callback']) ? (BASE_URL . $key) : '#') . '">' . 
									$item['options']['title'] .
								'</a>';
				// Recursively generate its subitems
				$args = array(
					'tree' => $variables['tree'][$key],
					'path' => $key,
					'current_depth' => 1,
					'max_depth' => $variables['max_depth'],
					'active_item' => $active_item
				);
				$output .= theme_menu_generate_submenu($args);
				$output .=  '</li>';
			} else {
				break;
			}
		}
		$output .= '</ul>';

		return $output;
	}

	/**
	 * Recursive helper function for generate_menu(). 
	 * Generates a submenu with depth indicator classes.
	 * @param $tree array. The tree to generate a
	 *  submenu from.
	 * @param $path string. The path until the item
	 *  where we are in the iteration (generated from
	 *  parent items).
	 * @param $current_depth int. The level (depth) of
	 *  where we are in the iteration.
	 * @param $max_depth int. The max amount of levels
	 *  that will be generated.
	 * @param $active_item array. Array of url parts
	 *  which indicate the current url. E.g.: 
	 *  http://test.com/test/test2 -> array('test', 
	 *  'test2')
	 */
	function theme_menu_generate_submenu(&$variables) {
		if ($variables['current_depth'] < $variables['max_depth']) {
			// If the item contains subitems
			if(count($variables['tree']) > 1) {
				$output = '<ul class="depth-' . $variables['current_depth'] . '">';
				foreach ($variables['tree'] as $key => $item) {
					$attributes = array();
		       		// Make sure the item is not the options array
					if ($key != 'options') {
						if (isset($item['options']['attributes'])) {
			 				$attributes = $item['options']['attributes'];
			 				// Make sure attribute value(s) is/are always an array
							foreach ($attributes as $attribute => $values) {
								if (is_string($values))
									$attributes[$attribute] = array($values);
							}
						}
						
						if(array_key_exists($variables['current_depth'], $variables['active_item']))
							$active_item_depth = implode('/', array_slice($variables['active_item'], 0, $variables['current_depth'] + 1));
						if (isset($active_item_depth) && $active_item_depth == ($variables['path'] . '/' . $key))
							$attributes['class'][] = 'active';
						$output .= '<li ' . (isset($attributes) ? Menu::html_attributes($attributes) : '') . '>' .
							// Print href only when a page callback is defined
							'<a href="' . (isset($item['options']['page callback']) ? (BASE_URL . $variables['path'] . '/' . $key) : '#') . '">' .
								$item['options']['title'] . 
							'</a>';
						// Recursively generate its subitems
						$args = array(
							'tree' => $variables['tree'][$key],
							'path' => ($variables['path'] . '/' . $key), 
							'current_depth' => $variables['current_depth'] + 1, 
							'max_depth' => $variables['max_depth'],
							'active_item' => $variables['active_item']
						);
						$output .= theme_menu_generate_submenu($args);
						$output .=  '</li>';
					}
				}
				$output .= '</ul>';
			}
		} else {
			return;
		}

		if(isset($output)) {
			return $output;
		}
	}



