<?php
	require_once 'includes/constants.inc';

	// Import settings.php and make these settings global
	require_once SITE_ROOT . 'settings.php';
	$GLOBALS['config'] = $config;

	// Write the base url to the system javascript
 	$filename = PLUGIN_PATH . 'system/js/system.js'; 
    $file_content = file($filename); 
    $x = count($file_content); 
    $fp = fopen($filename, "w+");  
    $file_content[3] = "var baseUrl = '" . BASE_URL . "';\n"; 
    $y = 0; 
    fwrite($fp, implode($file_content, ''));
    fclose($fp);  

    // Error reporting
	if(!$config['production_mode']) {
		ini_set("error_reporting", "true");
		error_reporting(E_ALL|E_STRCT);
	}

	// Import system classes
	require_once 'Database.php';
	require_once 'PluginManager.php';
	require_once 'ThemeManager.php';
	require_once 'Menu.php';
	require_once 'Page.php';
	require_once 'Templater.php';
	require_once 'UserManager.php';
	require_once 'User.php';

	// If the plugin table does not exists (representative 
	// for all tables), suggest to run install.php
	global $config;
	$schemas = Database::get_manager()->select(
		"SHOW TABLES FROM " . $config['db']['main']['dbname'] . " LIKE 'plugin'");
	if(!isset($schemas[0])) {
		echo '<h2>The database tables are not yet installed</h2>
			<form action="' . get_base_url() . '/install.php" method="get">
			    <input type="submit" value="Install database tables" name="Submit" />
			</form>';
		exit;
	}

	// Instantiate system classes
	// (the object variables are uppercase so  
	// they differentiate from normal variables)
	$PLUGINS = new PluginManager();
	$THEMES = new ThemeManager();
	$MENU = new Menu();
	$PAGE = new Page();
	$CONTENT = new Templater();
	$USERS = new UserManager();
	$USER = new User();

	// Load the theme template overrides
	include_once THEME_PATH . $THEMES->get_enabled_theme() . '/template.php';

	// Load the system plugin
	$PLUGINS->load_plugin('system');

	// Load other enabled plugins
	$active_plugins = $PLUGINS->get_active_plugins();
	if(!empty($active_plugins)) {
		foreach ($active_plugins as $active_plugin) {
			if($active_plugin != 'system')
				$PLUGINS->load_plugin($active_plugin);
		}
	}

	// Build up the menus
	$MENU->build_system_menu();
	$MENU->build_user_menu();

	
