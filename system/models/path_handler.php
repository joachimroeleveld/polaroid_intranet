<?php
	require_once 'includes/access_callbacks.inc';

	/**
	 * Handles each incoming page request.
	 */
	function handle_path($path) {
		global $PAGE;
		global $MENU;
		global $USER;

		// If not front
		if (!(get_url() == BASE_URL)) {
			// Get the menu item's options
			$options = $MENU->get_path_options($path);
		} else if (User::is_logged_in()) {
			// Get the front menu item's options
			$options = $MENU->get_path_options('front');
		} else {
			redirect_303(array('path' => 'user/login'));
		}
		
		path_check_user_access(null, $options, true);

		// If this menu item is undefined (or it's page callback)
		if (isset($options) && isset($options['page callback'])) {
			// Set the page title
			$PAGE->set_title($options['title']);

			// Call the page callback with it's arguments
			call_user_func($options['page callback'], (isset($options['page arguments']) ? $options['page arguments'] : array()));

			publish();
		} else {
			// Strip the last part of the url
			$url_parts = explode('/', $path);
			unset($url_parts[count($url_parts) - 1]);
			$url = implode('/', $url_parts);

			handle_path_helper($url);
		}
	}

	/**
	 * Check for a path if the logged in user has access 
	 * to it.
	 * @param $path string. The path to check
	 * @param $options array. When no path is provided, 
	 * 	one can instead directly provide the options 
	 *	of the menu item.
	 * @param $redirect boolean. Whether to redirect to
	 *	an error page if the user has no permission to
	 *	access the page.
	 */
	function path_check_user_access($path = null, $options = null, $redirect = false) {
		global $MENU;

		// If the path is set, get the menu item its options
		if (isset($path))
			$options = $MENU->get_path_options($path);

		// If the path is local (only locally available) and the visitor is not
		if (isset($options['access arguments']) && isset($options['access arguments']['local_page']) 
			&& $options['access arguments']['local_page'] && !visitor_is_local() && $redirect)
		{
			error_403('not_local_visitor');	
		}

		if (isset($options['access callback'])) {
			// If a permission is defined but the user is logged out
			if ($options['access callback'] == 'user_permission' && !User::is_logged_in()) {
				if ($redirect)
					error_not_logged_in();
				else
					return false;
			}

			// The admin user is allowed to access every path
			if (User::is_logged_in() && User::get_username() == 'admin' && 
				$options['access callback'] != 'logged_in' && $options['access callback'] != 'logged_out')
			{
				return true;	
			}

			// Call the access callback passing the access arguments along with it
			if (!call_user_func($options['access callback'], 
				(isset($options['access arguments']) ? $options['access arguments'] : array())))
			{
				if ($redirect) {
					switch ($options['access callback']) {
						case 'logged_in':
							error_not_logged_in();
							break;
						case 'logged_out':
							redirect_303();
							break;
						default:
							error_403();
					}
				} else {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Recursive function which checks if a smaller part
	 * of the url (url is broken down in pieces by the
	 * slash delimiter) can be used as a valid url.
	 * @param $path string. The path which parts should
	 * 	be checked.
	 */
	function handle_path_helper($path) {
		global $MENU;

		// Get the menu item's options
		$options = $MENU->get_path_options($path);

		// If this menu item is undefined (or it's page callback)
		if (!isset($options) || !isset($options['page callback'])) {
			$url_parts = explode('/', $path);

			// If this is not the last part of the url that can be checked
			if (count($url_parts) != 1) {
				// Strip the last part of the url
				unset($url_parts[count($url_parts) - 1]);
				$url = implode('/', $url_parts);

				// Recursively check whether a smaller part of the url can be used as a valid url
				handle_path_helper($url);
			} 
			// If the base url is correct
			else if (trim(substr(get_url(), 0, strlen(BASE_URL)), '/') == BASE_URL) {
				// Redirect to front
				redirect_303();
			} 
			// If the url is invalid (theoretically never reached)
			else {
				error_404();
			}
		} else {
			// Redirect to the valid part of the url
			redirect_303(array('path' => $path));
		}
	}

	/**
	 * Loads a plugin view file into the page body.
	 */
	function plugin_view($args) {
		global $CONTENT;
		global $PAGE;

		// Load the plugin's view file
		$CONTENT->load(PLUGIN_PATH . $args['plugin'] . '/views/' . $args['view'] . '.view.php');
		$CONTENT->set('plugin_view', $args['plugin']);
		$PAGE->bodyClasses[] = 'plugin-' . $args['plugin'];
	}

	/**
	 * Loads a view from the views folder in SITE_ROOT.
	 */
	function system_view($args) {
		global $CONTENT;
		global $PAGE;

		// Load the system's view file
		$CONTENT->load(SITE_ROOT . 'views/' . $args['view'] . '.view.php');
		$PAGE->bodyClasses[] = 'plugin-system';
	}

	/**
	 * Loads a system error view
	 */
	function system_error_view($args) {
		$args['view'] = 'error_pages/' . $args['view'];
		system_view($args);
	}

	/**
	 * Sets a page's body content and publishes it.
	 */
	function publish() {
		global $CONTENT;
		global $PAGE;

		$PAGE->set_body($CONTENT);
		$PAGE->publish();
	}

	/**
	 * Produces a 403 (forbidden) error giving
	 * the visitor feedback he/she must login.
	 */
	function error_not_logged_in() {
		error_403('not_logged_in');
	}

	/**
	 * Produces a 403 (forbidden) error.
	 */
	function error_403($error_view = '403') {
		global $PAGE;

		header('HTTP/1.0 403 Forbidden', true, 403);
		$PAGE->set_title('Error: 403');
		system_error_view(array('view' => $error_view));
    	publish();
    	exit;
	}

	/**
	 * Produces a 404 (not found) error.
	 */
	function error_404($error_view = '404') {
		global $PAGE;

		header("HTTP/1.0 404 Not Found", true, 404);
		$PAGE->set_title('Error: 404');
		system_error_view(array('view' => $error_view));
    	publish();
    	exit;
	}

	/**
	 * Redirect to a path with status code 303.
	 * @param $path string. An optional path. If not
	 * 	supplied, the front page is used.
	 */
	function redirect_303($args = null) {
		header('Location: ' . BASE_URL . (isset($args) ? ($args['path']) : ''), true, 303);
    	exit;
	}



