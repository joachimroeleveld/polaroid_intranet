<?php
	class ThemeManager
	{
		private $indexed_themes = array();
		private $enabled_theme = null;

		function __construct() {
			// Populate the list of indexed themes
			$this->indexed_themes = $this->index_themes();
			// Get the enabled theme from the database
			$enabled_theme_db = Database::get_manager()->select_quick(array('status' => 1), 'theme')[0]['name'];
			// If no theme is found in the database, enable one that is indexed.
			if (!isset($enabled_theme_db)) {
				Database::get_manager()->insert(array(
					'name' => $this->indexed_themes[0], 
					'status' => 1), 'theme'
				);
				$enabled_theme_db = $this->indexed_themes[0];
			}
			$this->enabled_theme = $this->indexed_themes[0];
			// Cleanup the theme database table
			$this->cleanup_themes_table();
		}

		/**
		 * Returns the return value of a override defined in a 
		 * template override of a theme.
		 * @param $theme string. The theme to get the override from.
		 * @param $override string. The override to call.
		 * @param $args object. Any arguments (will be passed to the 
		 *  override).
		 */
		static function theme_function_override_invoke($theme, $override, &$variables) {
			$function = $theme .'_'. $override;

			if (self::theme_function_override_exists($theme, $override))
		     	return call_user_func_array($function, array($variables));
		}

		/**
		 * Checks if a certain theme theme function override exists  
		 * in a template override of a theme.
		 * @param $theme string. The theme to check the override from.
		 * @param $override string. The specific theme function.
		 */
		static function theme_function_override_exists($theme, $override) {
		    return function_exists($theme .'_'. $override);
		}

		/**
		 * Calls a theme function. If the theme function is overridden 
		 * in a theme, use that implementation, otherwise use the 
		 * default implementation.
		 * @param $function string. The theme function to call.
		 * @param $variables array. All variables that should be 
		 * 	passed to the theme function.
		 */
		public function theme_function_invoke($function, &$variables) {
			if (self::theme_function_override_exists($this->enabled_theme, $function))
				// Call the theme override and use this over the default implementation.
		     	return self::theme_function_override_invoke($this->enabled_theme, $function, $variables);
		  	else
		  		// Call the default implementation of the theme function.
		  		return call_user_func_array('theme_' . $function, array($variables));
		}

		/**
		 * Scan the theme directory for any themes.
		 * An .theme file is required in order for the theme
		 * to be indexed.
		 */
		private function index_themes() {
			if ($handle = @opendir(THEME_PATH)) {
				while($theme = readdir($handle)) {
					if (is_dir(THEME_PATH . $theme) && ($theme != '.') && ($theme != '..') && 
						is_file(THEME_PATH . $theme . '/' . $theme . '.theme'))
					{
						$themes[] = $theme;
					}
				}
				
				closedir($handle);
			}

			return $themes;
		}

		/**
		 * Removes any theme from the database which does not exist
		 * anymore in the filesystem.
		 */
		private function cleanup_themes_table() {
			$db_themes = Database::get_manager()->select('SELECT * FROM theme');

			if (isset($db_themes)) {
				foreach ($db_themes as $theme) {
					if (!in_array($theme['name'], $this->indexed_themes))
						Database::get_manager()->delete_quick(array('name' => $theme['name']), 'theme');
				}
			}
		}

		/**
		 * Changes the current enabled theme.
		 * @param $theme string. The theme which to enable.
		 */
		public function switch_enabled_theme($theme) {
			$exists_in_theme_table = Database::get_manager()->select_quick(array('name' => $theme), 'theme');
			if (isset($exists_in_theme_table))
				// Set the theme's status in the database theme table to enabled
		    	Database::get_manager()->update_quick(array('status' => 1), array('name' => $theme), 'theme');
		    else
		    	// Create a record for the theme in the database theme table
		    	Database::get_manager()->insert(array('name' => $theme, 'status' => 1), 'theme');

		    // Disable all other themes (only one theme can be enabled)
		    Database::get_manager()->generic('UPDATE theme SET status = 0 WHERE name <> ?', array($theme));

			$this->enabled_theme = $theme;
		}

		public function get_indexed_themes() {
			return $this->indexed_themes;
		}

		public function get_enabled_theme() {
			return $this->enabled_theme;
		}
	}


