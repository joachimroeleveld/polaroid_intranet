<?php
  /********************************************************************
   * Adapted verion of the Drupal 7 schema API
   * API: http://goo.gl/MzOT4K
   * -Added foreign key functionality (is documented in the API)
   * -Added 'on update' and 'on delete' functionality to foreign keys.
   ********************************************************************/

  function create_table_sql($name, $table) {
    // Provide defaults if needed.
    $table += array(
      'mysql_engine' => 'InnoDB',
      'mysql_character_set' => 'utf8',
    );

    $sql = "CREATE TABLE `" . $name . "` (\n";

    // Add the SQL statement for each field.
    foreach ($table['fields'] as $field_name => $field) {
      $sql .= create_field_sql($field_name, process_field($field)) . ", \n";
    }

    // Process keys & indexes.
    $keys = create_keys_sql($table);
    if (count($keys)) {
      $sql .= implode(", \n", $keys) . ", \n";
    }

    // Remove the last comma and space.
    $sql = substr($sql, 0, -3) . "\n) ";

    $sql .= 'ENGINE = ' . $table['mysql_engine'] . ' DEFAULT CHARACTER SET ' . $table['mysql_character_set'];

    return $sql;
  }

  function process_field($field) {
    if (!isset($field['size'])) {
      $field['size'] = 'normal';
    }

    // Set the correct database-engine specific datatype.
    // In case one is already provided, force it to uppercase.
    if (isset($field['mysql_type'])) {
      $field['mysql_type'] = strtoupper($field['mysql_type']);
    }
    else {
      $map = get_field_type_map();
      $field['mysql_type'] = $map[$field['type'] . ':' . $field['size']];
    }

    if (isset($field['type']) && $field['type'] == 'serial') {
      $field['auto_increment'] = TRUE;
    }

    return $field;
  }

  function create_field_sql($name, $spec) {
    $sql = "`" . $name . "` " . $spec['mysql_type'];

    if (in_array($spec['mysql_type'], array('VARCHAR', 'CHAR', 'TINYTEXT', 'MEDIUMTEXT', 'LONGTEXT', 'TEXT'))) {
      if (isset($spec['length'])) {
        $sql .= '(' . $spec['length'] . ')';
      }
      if (!empty($spec['binary'])) {
        $sql .= ' BINARY';
      }
    }
    elseif (isset($spec['precision']) && isset($spec['scale'])) {
      $sql .= '(' . $spec['precision'] . ', ' . $spec['scale'] . ')';
    }

    if (!empty($spec['unsigned'])) {
      $sql .= ' unsigned';
    }

    if (isset($spec['not null'])) {
      if ($spec['not null']) {
        $sql .= ' NOT NULL';
      }
      else {
        $sql .= ' NULL';
      }
    }

    if (!empty($spec['auto_increment'])) {
      $sql .= ' auto_increment';
    }

    // $spec['default'] can be NULL, so we explicitly check for the key here.
    if (array_key_exists('default', $spec)) {
      if (is_string($spec['default'])) {
        $spec['default'] = "'" . $spec['default'] . "'";
      }
      elseif (!isset($spec['default'])) {
        $spec['default'] = 'NULL';
      }
      $sql .= ' DEFAULT ' . $spec['default'];
    }

    if (empty($spec['not null']) && !isset($spec['default'])) {
      $sql .= ' DEFAULT NULL';
    }

    return $sql;
  }

  function create_keys_sql($spec) {
    $keys = array();

    // Added foreign key functionality
    if (!empty($spec['foreign keys'])) {
      foreach ($spec['foreign keys'] as $relation => $fields) {
        $sources = '`' . implode('`, `', array_keys($fields['columns'])) . '`';
        $targets = '`' . implode('`, `', array_values($fields['columns'])) . '`';
        
        $keys[] = 'CONSTRAINT `' . $relation . '` FOREIGN KEY (' . $sources . ")\n" .
                  'REFERENCES `' . $fields['table'] . '` (' . $targets . ")" .
                  (isset($fields['on delete']) ? ('\nON DELETE ' . $fields['on delete']) : '') .
                  (isset($fields['on update']) ? ('\nON UPDATE ' . $fields['on update']) : '');
      }
    }
    if (!empty($spec['primary key'])) {
      $keys[] = 'PRIMARY KEY (' . create_keys_sql_helper($spec['primary key']) . ')';
    }
    if (!empty($spec['unique keys'])) {
      foreach ($spec['unique keys'] as $key => $fields) {
        $keys[] = 'UNIQUE KEY `' . $key . '` (' . create_keys_sql_helper($fields) . ')';
      }
    }
    if (!empty($spec['indexes'])) {
      foreach ($spec['indexes'] as $index => $fields) {
        $keys[] = 'INDEX `' . $index . '` (' . create_keys_sql_helper($fields) . ')';
      }
    }

    return $keys;
  }

  function create_keys_sql_helper($fields) {
    $return = array();
    foreach ($fields as $field) {
      if (is_array($field)) {
        $return[] = '`' . $field[0] . '`(' . $field[1] . ')';
      }
      else {
        $return[] = '`' . $field . '`';
      }
    }
    return implode(', ', $return);
  }

  function get_field_type_map() {
    $map = array(
      'varchar:normal' => 'VARCHAR',
      'char:normal' => 'CHAR',
      'text:tiny' => 'TINYTEXT',
      'text:small' => 'TINYTEXT',
      'text:medium' => 'MEDIUMTEXT',
      'text:big' => 'LONGTEXT',
      'text:normal' => 'TEXT',
      'serial:tiny' => 'TINYINT',
      'serial:small' => 'SMALLINT',
      'serial:medium' => 'MEDIUMINT',
      'serial:big' => 'BIGINT',
      'serial:normal' => 'INT',
      'int:tiny' => 'TINYINT',
      'int:small' => 'SMALLINT',
      'int:medium' => 'MEDIUMINT',
      'int:big' => 'BIGINT',
      'int:normal' => 'INT',
      'float:tiny' => 'FLOAT',
      'float:small' => 'FLOAT',
      'float:medium' => 'FLOAT',
      'float:big' => 'DOUBLE',
      'float:normal' => 'FLOAT',
      'numeric:normal' => 'DECIMAL',
      'blob:big' => 'LONGBLOB',
      'blob:normal' => 'BLOB',
    );

    return $map;
  }