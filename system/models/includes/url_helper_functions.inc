<?php
	/**
	 * Returns if HTTPS is enabled for the current url
	 */
	function https() {
	  return
	    (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
	    || $_SERVER['SERVER_PORT'] == 443;
	}

	/**
	 * Returns the user's IP e.g. 192.168.1.10
	 */
	function visitor_get_ip() {
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];

	    if (filter_var($client, FILTER_VALIDATE_IP)) {
	        $ip = $client;
	    } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
	        $ip = $forward;
	    } else {
	        $ip = $remote;
	    }

	    return $ip;
	}

	/**
	 * Returns whether the user comes from a LAN ip.
	 */
	function visitor_is_local() {
		return !filter_var(visitor_get_ip(), FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE);
	}

	/**
	 * Returns the base url e.g.:
	 * `http://localhost:8888/polaroid_intranet`
	 */
	function get_base_url() {
	    $proto = https() ? 'https://' : 'http://';
	    $base_url = $proto . $_SERVER['HTTP_HOST'];

	    // $_SERVER['SCRIPT_NAME'] can, in contrast to $_SERVER['PHP_SELF'], not
	    // be modified by a visitor.
	    if ($dir = rtrim(dirname($_SERVER['SCRIPT_NAME']), '\/')) {
	      $base_url .= $dir;
	    }

	    return $base_url;
	}

	/**
	 * Returns the full url of the current page e.g.:
	 * 
	 */
	function get_url() {
		$proto = https() ? 'https://' : 'http://';
		$host = $_SERVER['SERVER_NAME'];
		$port = ($_SERVER['SERVER_PORT'] == 80 ? '' : ':' . $_SERVER['SERVER_PORT']);
		$dir = preg_replace("/\?.*/", '', $_SERVER['REQUEST_URI']);
		
		return "$proto$host$port$dir";
	}

	/**
	 * Returns the path relative to the base url e.g.:
	 * `portal/home`
	 */
	function get_path() {
		$path = substr(get_url(), strlen(BASE_URL));

		return ltrim($path, '/');
	}