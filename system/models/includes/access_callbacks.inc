<?php
	function user_permission($args) {
		return User::has_permission($args['permissions']);
	}

	function logged_in() {
		return User::is_logged_in();
	}

	function logged_out() {
		return !User::is_logged_in();
	}