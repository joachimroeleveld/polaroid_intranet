<?php
	require_once 'url_helper_functions.inc';

 	define("BASE_URL", get_base_url() . '/');
 	define("MODEL_PATH", SITE_ROOT . 'system/models/');
 	define("VIEW_PATH", SITE_ROOT . 'views/');
 	define("THEME_PATH", SITE_ROOT . 'themes/');
 	define("PLUGIN_PATH", SITE_ROOT . 'plugins/');
	define("FILE_PATH", SITE_ROOT . 'files/');
 	define("PLUGIN_URL", BASE_URL . 'plugins/');
	define("FILE_URL", BASE_URL . 'files/');
	define("THEME_URL", BASE_URL . 'themes/');