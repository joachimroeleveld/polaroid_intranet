<?php
    /**
     * Adapted version of: https://github.com/techjacker/PDO-Quick/blob/master/pdoquick.php
     */
    class Database 
    {
        // VARIABLES

        private static $_dbManagerInstance;
        private $_dbConnectionInstance;
        public $_func = "name_params";
        private $_statement_number = 0;
        private $_dbtype = "mysql";

        // PDO constants options: http://php.net/manual/en/pdo.constants.php
        // Increase performance by creating a persistent connection
        protected $_db_params = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8", PDO::ATTR_PERSISTENT => true);


        // CONNECTION METHODS

        public static function get_manager(){
            if (null === self::$_dbManagerInstance) {
                self::$_dbManagerInstance = new Database();
            }
            return self::$_dbManagerInstance;
        }

        private function __construct() {
            if(!$this->_dbConnectionInstance) {
                try {
                    $dbdata = $GLOBALS['config']['db']['main'];
                    $this->_dbConnectionInstance =  new PDO($this->_dbtype.':host='.$dbdata['host'].';port='.$dbdata['port'].';dbname='.$dbdata['dbname'], $dbdata['username'], $dbdata['password'], $this->_db_params);

                    if ($GLOBALS['config']['production_mode'] === true) {
                        $this->_dbConnectionInstance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
                    } else {
                       $this->_dbConnectionInstance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    }
                } catch (PDOException $e) {
                    $this->_dbConnectionInstance = null;
                
                    // Error message output
                    if ($GLOBALS['config']['production_mode'] === true) {
                        // Log the errors
                        file_put_contents(SITE_ROOT . 'logs/database_errors.log', $e->getMessage(), FILE_APPEND);
                        print $this->return_error("An error occured when instantiating the database connection.");
                        exit;
                    } else {
                        print $this->return_error($e->getMessage())['error'];
                        exit;
                    }
                }
            }

            return $this->_dbConnectionInstance;
        }

        public function __destruct(){
           $this->_dbConnectionInstance = null;
        }

        private function return_error($error) {
            return array('error' => $error);
        }


        // CUSTOM SQL QUERY METHODS

        /* 
         * Use for delete and update statements
         * Usage example: 
         * $table       = 'City';
         * $sql         = "UPDATE $table SET District = ? WHERE Population > ? AND CountryCode = ?" ;
         * $params       = array('Hertfordshire', 421010, 'NLD');
         *
         * Database::get_manager()->generic($sql, $params);
        */
        public function generic($sql, $values=null) {
            $statement = $this->apply($sql, $values);
            
            if (is_array($statement) && isset($statement['error']))
                return $statement;
        }


        // QUERY METHODS

        /* 
         * Usage example: 
         * $table   = 'City';
         * $sql     = "SELECT * FROM $table WHERE CountryCode = ? AND District = ? LIMIT 100" ;
         * $params  = array('NLD', 'Utrecht');
         * 
         * Database::get_manager()->select($sql, $params);
        */
        public function select($sql, $values=null) {
            $statement = $this->apply($sql, $values);
            if (is_array($statement) && isset($statement['error']))
                return $statement;

            while ( $row = $statement->fetch(PDO::FETCH_ASSOC) ) {
                $result[] = $row;
            }

            if (isset($result))
                return $result;
        }

        /* 
         * Usage example: 
         * $table   = 'City';
         * $row    = array(
         *   'Name'         => 'Farringdon',
         *   'CountryCode'  => 'GB',
         *   'District'     => 'London',
         *   'Population'   => 404561
         * );
         *
         * Database::get_manager()->insert($row, $table);
        */
        public function insert($data_array_unchecked, $table) {
            $data_array = array();
            foreach ($data_array_unchecked as $k => $v) {
                if ($v != null) { $data_array[$k] = $v; }
            }

            $cols = $this->named_columns($data_array);
            $vals = $this->named_values($data_array);

            $sql = " INSERT INTO $table ( $cols ) values ( $vals ) ";

            $statement = $this->apply($sql, $data_array);
            if (is_array($statement) && isset($statement['error']))
                return $statement;

            return $this->_dbConnectionInstance->lastInsertId();
        }


        // QUICK QUERY METHODS

        /* 
         * Usage example: 
         * $table           = 'City';
         * $limit           = 10;
         * $where_equals    = array(
         *   'CountryCode'  => 'NLD',
         *   'District'     => 'Zuid-Holland'
         * );
         *
         * Database::get_manager()->select_quick($where_equals, $table, $limit);
        */
        public function select_quick($where_equals, $table, $limit=1000) {
            $sql_where = $this->where_equals("SELECT * FROM $table", $where_equals);

            $statement = $this->apply($sql_where[0]." LIMIT $limit", $sql_where[1]);
            if (is_array($statement) && isset($statement['error']))
                return $statement;

            while ( $row = $statement->fetch(PDO::FETCH_ASSOC) ) {
                $result[] = $row;
            }

            if (isset($result))
                return $result;
        }

        /* 
         * Usage example: 
         * $table           = 'City';
         * $where_equals    = array(
         *   'CountryCode'  => 'NLD',
         *   'District'     => 'Zuid-Holland'
         * );
         *
         * Database::get_manager()->delete_quick($where_equals, $table);
        */
        public function delete_quick($where_equals, $table) {
            $sql_where = $this->where_equals("DELETE FROM $table", $where_equals);

            $statement = $this->apply($sql_where[0], $sql_where[1]);
            if (is_array($statement) && isset($statement['error']))
                return $statement;
        }

        /* 
         * Usage example: 
         * $table           = 'City';
         * $new_values      = array(
         *   'Population'   => 421018, 
         *   'District'     => 'Dorset' 
         * );
         * $where_condition = array(
         *   'CountryCode' => 'NLD', 
         *   'Name' => 'Amsterdam'
         * );
         *
         * Database::get_manager()->update_quick($new_values, $where_condition, $table);
        */
        public function update_quick($new_values, $where_equals, $table) {
            $sql = "UPDATE $table SET";
            $where = array();
            foreach ($new_values as $f => $v) {
                $sql .= " $f=?,";
                $where[] = $v;
            }
            $sql = rtrim($sql, ",");

            $sql_where = $this->where_equals($sql, $where_equals);
            foreach ($sql_where[1] as $value) {
                $where[] = $value;
            }
          
            $statement = $this->apply($sql_where[0], $where);
            if (is_array($statement) && isset($statement['error']))
                return $statement;
        }


        // HELPER FUNCTIONS

        public function where_equals($sql, $where_condition) {
            $p = 0;
            $count = count($where_condition);
            $values = array();

            foreach ($where_condition as $k => $v) {
                $p++;
                $sql .= ($p == 1) ? " WHERE " : "";
                $sql .= "$k=?";
                $sql .= ($p >= 1) && ($p < $count) ? " AND " : "";
                $values[] = $v; // append where id value to values array
            }

            $sql_where = array($sql, $values);
            return $sql_where;
        }

        private function apply($prepare, $execute) {
            $statement_name = 'statement'.$this->_statement_number;
            $this->_statement_number++;

            try {
                $this->_dbConnectionInstance->beginTransaction();
                ${$statement_name} = $this->_dbConnectionInstance->prepare($prepare);
                if ($this->_statement_number != 1) { ${$statement_name}->closeCursor(); }

                ${$statement_name}->execute($execute);
                $this->_dbConnectionInstance->commit();

                return ${$statement_name};
                ${$statement_name} = null;
            } catch(PDOException $e) {
                $this->_dbConnectionInstance->rollBack();
                return $this->return_error($e->getMessage());
            }
        }

        public function named_columns($data_array) {
            return $insert_columns = implode(", ", array_keys($data_array));
        }

        function name_params($n) { return ":".$n; }

        public function named_values($data_array) {
            return $insert_values = implode(", ", array_map(array($this, $this->_func), array_keys($data_array)));
        }

    }