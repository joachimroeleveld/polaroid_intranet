<?php
    /**
     * Class login
     * handles the user's login, logout and registration process
     *
     * NOTE: in this class, password_* functions from PHP 5.5
     * are used. When using a PHP version older than 5.5, you
     * have to include password.inc from the includes folder:
     * require_once 'includes/password.inc';
     */
    class User
    {
        public $error = array();
        public $message = array();
        /**
         * Salt with which passwords are stored in the cookie
         */
        private static $salt = 'fr7eNbk$';

        public function __construct() {
            session_start();
            self::check_login_cookie();
        }

        /**
         * Checks if the visitor, when not already logged in, has 
         * a valid cookie with which an autologin can be 
         * performed.
         */
        public static function check_login_cookie() {
            // If logged in and cookie is present
            if (!self::is_logged_in() && isset($_COOKIE['secret'])) {
                // Decode cookie
                $cookie_decoded = base64_decode($_COOKIE['secret']);
                // Disassembly the cookie string by the `:` delimiter
                list($uid, $hashed_password) = explode(':', $cookie_decoded);
                // Get the corresponding user from the database
                $user = Database::get_manager()->select_quick(
                        array('uid' => $uid), 'user', 1);

                // Check if the passwords match
                if (isset($user[0]) && md5($user[0]['password'], self::$salt) == $hashed_password) {
                    // Store the session variable (log in)
                    $_SESSION['username'] = $user[0]['username'];
                } else {
                    // This cookie is invalid (propably malicious). 
                    // Therefore, we unset it.
                    setcookie('secret', '', 1, '/');
                }
            }
        }

        /**
         * Checks if the user has any of the passed permissions.
         * @param $permissions array. The array with permissions.
         */
        public static function has_permission($permissions) {
            $user_db = Database::get_manager()->select_quick(array('username' => self::get_username()), 'user');
            // Look up the roles the user has.
            $roles_db = Database::get_manager()->select_quick(array('uid' => $user_db[0]['uid']), 'user_role');

            foreach ($permissions as $permission) {
                $permission_db = Database::get_manager()->select_quick(array('name' => $permission), 'permission');

                foreach ($roles_db as $key => $value) {
                    // Check if the role is associated with the permission
                    $role_permission = Database::get_manager()->select_quick(
                        array(
                            'pid' => $permission_db[0]['pid'], 
                            'rid' => $value['rid'],
                        ), 
                        'role_permission'
                    );

                    // Return true if ANY permission is associated 
                    // with one of the user's roles
                    if(isset($role_permission[0])) {
                        return true;
                    }
                }
            }
        }

        public function login($username, $password) {
            // Check login form contents
            if (empty($username)) {
                $this->error = "Username field is empty.";
            } elseif (empty($password)) {
                $this->error = "Password field is empty.";
            } elseif (!empty($username) && !empty($password)) {
                // Query user (if exists) from the database
                $user = Database::get_manager()->select_quick(
                    array('username' => $username), 'user', 1);

                // If user exists
                if (isset($user[0])) {
                    $user = $user[0];

                    // Using PHP 5.5's password_verify() function to check if 
                    // the provided password fits the hash of that user's password
                    if (password_verify($password, $user['password'])) {
                        $_SESSION['username'] = $username;

                        // Set cookie to a hash of the hashed password and a salt
                        $cookie = base64_encode($user['uid'] . ":" . md5($user['password'], self::$salt));
                        setcookie('secret', $cookie, strtotime('+30 days'), '/');

                        $this->message = "You have been successfully logged in.";
                        return true;
                    } else {
                        $this->error = "Wrong password. Try again.";
                    }
                } else {
                    $this->error = "This user does not exist.";
                }
            }

            return false;
        }

        public function logout() {
            session_unset();
            session_destroy();
            setcookie('secret', '', 1, '/');
            $this->message = "You have been logged out.";

            return true;
        }

        public static function is_logged_in() {
            return isset($_SESSION['username']);
        }

        public static function get_username() {
            return $_SESSION['username'];
        }
    }




