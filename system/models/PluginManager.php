<?php
	require_once 'PluginInstaller.php';

	class PluginManager 
	{
		private $active_plugins = array();

		function __construct() {
			// Load the enabled plugins from the database
			$active_plugins = Database::get_manager()->select_quick(array('status' => 1), 'plugin');

			// If no plugin is enabled, install the system plugin
			if (!isset($active_plugins[0])) {
				$this->enable_plugin('system');
				// Enable the plugin which defines the frontpage
				global $config;
				$this->enable_plugin($config['frontpage_plugin']);
			}

			// Store the enabled plugins in the active_plugins array
			if (isset($active_plugins)) {
				foreach ($active_plugins as $plugin) {
					$this->active_plugins[] = $plugin['name'];
				}
			}
		}

		/**
		 * Returns the path of the given plugin.
		 * @param string $plugin. The name of the plugin.
		 * @return string. Path of the plugin.
		 * @return false. The plugin with the given name does not exists.
		 */
		static function get_plugin_path($plugin) {
			if (is_dir(PLUGIN_PATH . $plugin) && is_file(PLUGIN_PATH . $plugin . '/' . $plugin . '.plugin')) {
				return PLUGIN_PATH . $plugin . '/';
			}
		}

		/**
		 * Returns an array of plugins that exist in the plugin folder (a .plugin file must exist).
		 */
		static function get_plugins() {
			if ($handle = @opendir(PLUGIN_PATH)) {		
				while($plugin = readdir($handle)) {
					$install_file = self::get_plugin_path($plugin);
					if (is_dir(PLUGIN_PATH . $plugin) && ($plugin != '.') && ($plugin != '..')
						&& ($plugin != 'system') && isset($install_file)) 
					{
						$plugins[] = $plugin;
					}
				}
				
				closedir($handle);
			}

			return $plugins;
		}

		/**
		 * Returns the return value of a hook defined in a plugin.
		 * @param $plugin string. The plugin to get the hook from.
		 * @param $hook string. The hook to call.
		 * @param $args object. Any arguments (will be passed to the hook).
		 */
		static function plugin_invoke() {
			$args = func_get_args();
			$plugin = $args[0];
			$hook = $args[1];
			unset($args[0], $args[1]);
			$function = $plugin .'_'. $hook;

			if (self::plugin_hook_exists($plugin, $hook)) {
		     	return call_user_func_array($function, $args);
		  	}
		}

		static function plugin_hook_exists($plugin, $hook) {
		    return function_exists($plugin .'_'. $hook);
		}

		static function get_uninstallable_plugins() {
			$result = Database::get_manager()->select('SELECT * FROM plugin');

			foreach ($result as $plugin) {
				if (self::check_uninstallable($plugin['name']))
					$plugins[] = $plugin['name'];
			}

			if(isset($plugins))
				return $plugins;
		}

		static private function check_uninstallable($plugin) {
			$result = Database::get_manager()->select_quick(array('name' => $plugin, 'status' => 0), 'plugin');
			$permissions = Database::get_manager()->select_quick(array('plugin' => $plugin), 'permission', 1);
			$schema = PluginInstaller::get_schema($plugin);

			// Check if the plugin exists in the plugin table, if it is not active, if HOOK_schema 
			// in the .install file exists and if any tables are remaining in the database
			return (isset($result) && 
				((isset($schema) && PluginInstaller::get_schema_remains($schema)) || isset($permissions)));
		}

		/**
		 * Loads plugin from a specific folder, includes a plugin's *.plugin file
		 */
		public function load_plugin($plugin) {
			if (in_array($plugin, $this->active_plugins)) {
				$plugin_file = self::get_plugin_path($plugin) . $plugin . '.plugin';
				require_once $plugin_file;
			}
		}

		/**
		 * Loads plugins from a specific folder, includes *.plugin files
		 */
		public function load_plugins() {
			$plugins = $this->get_plugins();

			foreach ($plugins as $plugin) {
				$this->load_plugin($plugin);
			}
		}

		public function enable_plugin($plugin) {
			$exists_in_plugin_table = Database::get_manager()->select_quick(array('name' => $plugin), 'plugin');
			if (isset($exists_in_plugin_table))
				// Set the plugin's status in the database plugin table to enabled
		    	Database::get_manager()->update_quick(array('status' => 1), array('name' => $plugin), 'plugin');
		    else
		    	// Create a record for the plugin in the database plugin table
		    	Database::get_manager()->insert(array('name' => $plugin, 'status' => 1), 'plugin');

		    // Add the plugin to the active_plugins array (instance variable).
			$this->active_plugins[] = $plugin;

			// Install the plugin (requires)
			if(is_file(PLUGIN_PATH . $plugin . '/' . $plugin . '.install')) {
				$installer = new PluginInstaller($plugin, $this);
				$installer->install();
			}
		}

		public function disable_plugin($plugin) {
			// Set the plugin's status in the database plugin table to disabled
		    Database::get_manager()->update_quick(array('status' => 0), array('name' => $plugin), 'plugin');
		    // Remove the plugin from the active_plugins array (instance variable).
		    $this->active_plugins = array_diff($this->active_plugins, array($plugin));
		}

		public function uninstall_plugin($plugin) {
			if(self::check_uninstallable($plugin)) {
				$installer = new PluginInstaller($plugin, $this);
				$installer->uninstall();
			}
		}

		public function get_active_plugins() {
			return $this->active_plugins;
		}

	}


