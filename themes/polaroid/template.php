<?php
	/****************************************************
	 * THEME FUNCTION OVERRIDES							*
	 ****************************************************/

	/**
	 * Overrides theme_menu_generate()
	 * ---
	 * Generates an HTML menu (<ul>, <li> structure).
 	 * @param $tree array. The menu tree object.
	 * @param $max_depth int. The max amount of levels 
	 *  of the tree that will be generated.
	 * @param $length int. The max amount of top tree 
	 *  items that will be generated.
	 */
	function polaroid_menu_generate(&$variables) {
		$length_counter = 0;
		$active_item = explode('/', get_path());
		unset($variables['tree']['front']);

		$output = '<ul class="nav navbar-nav menu">';
		foreach ($variables['tree'] as $key => $item) {
			$attributes = array();
			$subitems_count = count($variables['tree'][$key]);
			$has_subitems = $subitems_count > 1;
			
			// Check if the item has only one subitem and
			// if it is equal (same title) to it's parent.
			$new_url = null;
			if ($subitems_count == 2) {
				// Get the subitem's key
				$deep_copy = $item;
				unset($deep_copy['options']);
				$subitem_key = key($deep_copy);

				$has_duplicate_subitem = $item['options']['title'] == $deep_copy[$subitem_key]['options']['title'];

				if ($has_duplicate_subitem) {
					// Append the subitem's url to the current url (key) to get the new url
					$new_url = $key . '/' . $subitem_key;
					// Merge the options array so we get the page callback etc. from the subitem
					$item['options'] = array_merge($item[$subitem_key]['options'], $item['options']);
					// Unset the subitem
					unset($item[$subitem_key]);
					$has_subitems = false;
				}
			}

			if (isset($item['options']['attributes'])) {
 				$attributes = $item['options']['attributes'];
 				// Make sure attribute value(s) is/are always an array
				foreach ($attributes as $attribute => $values) {
					if (is_string($values))
						$attributes[$attribute] = array($values);
				}
			}
			if ($has_subitems)
				$attributes['class'][] = 'dropdown';
			if ($active_item[0] == $key)
				$attributes['class'][] = 'active';

			if ($length_counter < $variables['length']) {
				$length_counter++;

				$output .= '<li ' . (isset($attributes) ? Menu::html_attributes($attributes) : '') . '>' .
								// Print href only when a page callback is defined
								'<a href="' . (isset($item['options']['page callback']) ? (BASE_URL . (isset($new_url) ? $new_url : $key)) : '#') . '"' . ($has_subitems ? ' class="dropdown-toggle" data-toggle="dropdown"' : '') . '>' .
									$item['options']['title'] . ($has_subitems ? ' <b class="caret"></b>' : '') .
								'</a>';
				// Recursively generate its subitems
				$args = array(
					'tree' => $variables['tree'][$key],
					'path' => $key,
					'current_depth' => 1,
					'max_depth' => $variables['max_depth'],
					'active_item' => $active_item
				);
				$output .= polaroid_menu_generate_submenu($args);
				$output .=  '</li>';
			} else {
				break;
			}
		}
		$output .= '</ul>';

		return $output;
	}

	/**
	 * Overrides theme_menu_generate_submenu()
	 * ---
	 * Recursive helper function for generate_menu(). 
	 * Generates a submenu with depth indicator classes.
	 * @param $tree array. The tree to generate a
	 *  submenu from.
	 * @param $path string. The path until the item
	 *  where we are in the iteration (generated from
	 *  parent items).
	 * @param $current_depth int. The level (depth) of
	 *  where we are in the iteration.
	 * @param $max_depth int. The max amount of levels
	 *  that will be generated.
	 * @param $active_item array. Array of url parts
	 *  which indicate the current url. E.g.: 
	 *  http://test.com/test/test2 -> array('test', 
	 *  'test2')
	 */
	function polaroid_menu_generate_submenu(&$variables) {
		if ($variables['current_depth'] < $variables['max_depth']) {
			// If the item contains subitems
			if(count($variables['tree']) > 1) {
				$output = '<ul class="dropdown-menu depth-' . $variables['current_depth'] . '">';
				foreach ($variables['tree'] as $key => $item) {
					$attributes = array();
			       	// Make sure the item is not the options array
					if ($key != 'options') {
						// If this is the user menu, add a custom item to the tree in which the current logged in user is shown.
						global $USER;
						if ($variables['path'] == 'user' && $USER->is_logged_in()) {
							$output .= '<li class="username"><em>Logged in as</em> <strong>' . $USER->get_username() . '</strong></a></li>';
						}

						if (isset($item['options']['attributes'])) {
			 				$attributes = $item['options']['attributes'];
			 				// Make sure attribute value(s) is/are always an array
							foreach ($attributes as $attribute => $values) {
								if (is_string($values))
									$attributes[$attribute] = array($values);
							}
						}
						
						if(array_key_exists($variables['current_depth'], $variables['active_item']))
							$active_item_depth = implode('/', array_slice($variables['active_item'], 0, $variables['current_depth'] + 1));
						if (isset($active_item_depth) && $active_item_depth == ($variables['path'] . '/' . $key))
							$attributes['class'][] = 'active';

						$output .= '<li ' . (isset($attributes) ? Menu::html_attributes($attributes) : '') . '>' .
							// Print href only when a page callback is defined
							'<a href="' . (isset($item['options']['page callback']) ? (BASE_URL . $variables['path'] . '/' . $key) : '#') . '">' . 
								$item['options']['title'] . 
							'</a>';
						// Recursively generate its subitems
						$args = array(
							'tree' => $variables['tree'][$key],
							'path' => ($variables['path'] . '/' . $key), 
							'current_depth' => $variables['current_depth'] + 1, 
							'max_depth' => $variables['max_depth'],
							'active_item' => $variables['active_item']
						);
						$output .= polaroid_menu_generate_submenu($args);
						$output .=  '</li>';
					}
				}
				$output .= '</ul>';
			}
		} else {
			return;
		}

		if(isset($output)) {
			return $output;
		}
	}



