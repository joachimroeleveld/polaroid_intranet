// WHEN DOCUMENT IS READY

$(document).ready(function() {
	// Unbind system implementation
	$('body.plugins').off('click', '.uninstall-plugin');

	$('body.plugins').on('click', '.uninstall-plugin', function(){
		var button = $(this);
		var plugin = button.closest('tr').find('td.name').text();
	
		BootstrapDialog.confirm('Are you sure you want to uninstall this plugin? <strong>All associated data will be lost.</strong> This action cannot be undone.', function(result){
            if(result) {
            	controllerPost('uninstall_plugin', {plugin: plugin}, function(){
            		button.closest('tr').remove();
            	});
            }
        });
	});
});