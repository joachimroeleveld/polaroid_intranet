// GLOBAL VARIABLES

// This variable is updated by bootstrap.php on each page request
var baseUrl = 'http://localhost:8888/polaroid_intranet/';

var paths = {
    'controllers' : baseUrl + 'system/controllers.php',
    'plugins' : baseUrl + 'plugins'
};

// This script may not be functioning properly when this option is enabled!
var debugging = false;

if (debugging) {
	dataType = false;
} else {
	dataType = 'json';
}


// SELECTOR VARIABLES

var messageContainer = '#message-container';
var message = messageContainer + ' .message';


//GLOBAL FUNCTIONS

function getPluginPath(plugin) {
	return paths['plugins'] + '/' + plugin + '/';
}

function controllerPost(controllerFunction, argData, successCallback, controllersPath) {
	// If the nonnessecary arguments are undefined, give them a valid default value.
	argData = ((typeof argData === "undefined") || argData == null) ? {} : argData;
	successCallback = ((typeof successCallback === "undefined") || successCallback == null) ? function(){} : successCallback;
	controllersPath = ((typeof controllersPath === "undefined") || controllersPath == null) ? paths['controllers'] : controllersPath;

	$.ajax({
		type: 'POST',
		url: controllersPath,
		data: { 
			function: controllerFunction,
			data: argData
		},
		dataType: dataType,
		success: function(data, textStatus, jqXHR) {
			postSucces(data, textStatus, jqXHR, controllerFunction, successCallback);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			postFail(jqXHR, textStatus, errorThrown);
        },
	})
}

function postSucces(data, textStatus, jqXHR, controllerFunction, successCallback) {
	if(!debugging) {
		if (typeof data.error === 'undefined') {
			if (typeof data.warnings !== 'undefined') {
				var warnings = '';
				$.each(data.warnings, function(key, warning) {
					warnings = warnings + warning + '<br/>';
				});
    			alertMessage(warnings, "warning");
    		} else if (typeof data.messages !== 'undefined') {
				var messages = '';
				$.each(data.messages, function(key, message) {
					messages = messages + message + '<br/>';
				});
    			alertMessage(messages, "success");
    		}
    		
    		successCallback(data.data);
		} else {
			alertMessage(data.error, "danger");
		}
	} else {
		alertMessage("Debugging is turned on. Check the console.", "info");
		console.log("DEBUG: return data from function '" + controllerFunction + "': " + data);
		successCallback(data.data);
	}
}

function postFail(jqXHR, textStatus, errorThrown) {
	alertMessage(errorThrown, "danger");
	console.log('ERROR: ' + textStatus + ', ' + errorThrown);
}

// @param type = success || info || warning || danger
function alertMessage(text, type) {
	$(message).remove();
	$(messageContainer)
		.append('<div class="message alert alert-' + type + ' fade in"></div>')
		.hide()
		.fadeIn(150);
	$(message).alert();
	setAlertMessageText(text);
}

var alertTimeout;

function setAlertMessageText(text) {
	window.clearTimeout(alertTimeout);
	$(message).html('<span class="close" data-dismiss="alert">&times;</span>' + text);
	alertTimeout = setTimeout(function(){
		$(message).alert('close');
	}, 3000);
}


// WHEN DOCUMENT IS READY

$(document).ready(function() {
	// Login form submit
	$('body.plugin-system.login form[name="login"]').submit(function(event) {
		// Prevent default submit
		event.preventDefault();
		
		controllerPost(
			'login',
			{ data: $(this).serialize() },
			function() {
				// Redirect to front
				window.location.replace(baseUrl);
			}
		);
	});

	// Enable tooltips
	$('[data-toggle="tooltip"]').tooltip();

	// Plugin state toggle
	$('body.plugin-system.plugins').on('change', '.state-check', function(){
		var plugin = $(this).closest('tr').find('td.name').text();

		controllerPost(
			'save_plugin', 
			{
				plugin: plugin,
				state: ($(this).is(":checked") ? '1' : '0'),
			},
			null
		);
	});

	// Theme change
	$('body.plugin-system.themes').on('change', '.state-check', function() {
		var theme = $(this).closest('tr').find('td.name').text();

		if($(this).is(":checked")) {
			controllerPost(
				'save_theme', 
				{ theme: theme },
				null
			);
		}
	});

	// Permission toggle save
	$('body.plugin-system.permissions').on('change', 'input[type="checkbox"].permission-role-check', function() {
		// The role id and permission id can be obtained from the name attribute.
		var splitted = $(this).attr('name').split("-");
	
    	controllerPost(
    		'save_permission', 
    		{
    			rid: splitted[1],
    			pid: splitted[3],
    			state: ($(this).is(":checked") ? 1 : 0),
    		},
    		null
    	);
	});

	// Add role save
	$('body.plugin-system.roles #add-role-modal').on('click', 'button.submit', function() {
		var modal = $(this).closest('#add-role-modal');
		var tbody = modal.next('table#roles').children('tbody');
		var form = $(this).closest('.modal-footer').prev('.modal-body').find('form');

    	controllerPost('add_role', {data: form.serialize()}, function(data){
    		// Append the new role to the table
			controllerPost(
				'system_view_return',
				{
					view: 'role/role',
					data: {
						rid: data.rid,
					},
				}, function(data) {
					tbody.append(data.buffer);
					// Clear form fields
					$(form).trigger('reset');
    			}
    		);
    	});

    	// Close modal
		$(this).closest('#add-role-modal').modal('hide');
	});

	// Role delete
	$('body.plugin-system.roles #roles').on('click', '.delete-role[data-action="delete"]', function() {	
		var button = $(this);

    	controllerPost('delete_role', {rid: $(this).attr('data-rid')}, function(){
    		// Delete the associated table row
    		$(button).closest('tr').remove();
    	});
	});

	// Role edit modal
	$('body.plugin-system.roles #roles').on('click', 'button.edit-role[data-action="edit"]', function() {
		var button = $(this);

		// If modal already exists, remove it
		$(this).next('.modal').remove();

		// Get edit modal
		controllerPost(
			'system_view_return',
			{
				view: 'role/role_edit',
				data: {
					rid: $(this).attr('data-rid'),
					rolename: $(this).attr('data-role-name'),
				},
			}, function(data) {
				// Place modal html and show it
				button.after(data.buffer);
				button.next('.modal').modal('show');
			}
		);
	});

	// Role edit save
	$('body.plugin-system.roles #roles').on('click', '.edit-role-modal button.submit', function() {
		var button = $(this);
		// Serialize form data
		var form = $(this).closest('.modal-footer').prev('.modal-body').find('form').serialize();

    	controllerPost('edit_role', {data: form, rid: $(this).attr('data-rid')}, function(){
    		var row = button.closest('tr');
    		var tbody = row.parent('tbody');

    		// bug workaroud -> modal background does not  
    		// disappear after the row is removed, so we have
    		// to do it manually
    		$('body').children('.modal-backdrop').fadeOut(function(){
	    		// Hide the old table row
	    		row.remove();
    		});
    		// Append the new role to the table
			controllerPost(
				'system_view_return',
				{
					view: 'role/role',
					data: {
						rid: button.attr('data-rid'),
					},
				}, function(data) {
					tbody.append(data.buffer);
    			}
    		);
    	});
    	
    	// Close modal
		button.closest('.edit-role-modal').modal('hide');
	});

	// Add user save
	$('body.plugin-system.users #add-user-modal').on('click', 'button.submit', function() {
		var modal = $(this).closest('#add-user-modal');
		var tbody = modal.next('table#users').children('tbody');
		var form = $(this).closest('.modal-footer').prev('.modal-body').find('form');
	
    	controllerPost('add_user', {data: form.serialize()}, function(data){
    		// Append the new user to the table
			controllerPost(
				'system_view_return',
				{
					view: 'user/user',
					data: {
						uid: data.uid,
					},
				}, function(data) {
					tbody.append(data.buffer);
					// Clear form fields
					$(form).trigger('reset');
    			}
    		);
    	});

    	// Close modal
		$(this).closest('#add-user-modal').modal('hide');
	});

	// User delete
	$('body.plugin-system.users #users').on('click', '.delete-user[data-action="delete"]', function() {	
		var button = $(this);

    	controllerPost('delete_user', {uid: $(this).attr('data-uid')}, function(){
    		// Remove the associated table row
    		$(button).closest('tr').remove();
    	});
	});

	// User edit modal
	$('body.plugin-system.users #users').on('click', 'button.edit-user[data-action="edit"]', function() {
		var button = $(this);

		// If modal already exists, remove it
		$(this).next('.modal').remove();

		// Get edit modal
		controllerPost(
			'system_view_return',
			{
				view: 'user/user_edit',
				data: {
					uid: $(this).attr('data-uid'),
					username: $(this).attr('data-username'),
				},
			}, function(data) {
				// Place modal html and show it
				button.after(data.buffer);
				button.next('.modal').modal('show');
			}
		);
	});	

	// User edit save
	$('body.plugin-system.users #users').on('click', '.edit-user-modal button.submit', function() {
		var button = $(this);
		// Serialize form data
		var form = $(this).closest('.modal-footer').prev('.modal-body').find('form').serialize();

    	controllerPost('edit_user', {data: form, uid: $(this).attr('data-uid')}, function(){
    		var row = button.closest('tr');
    		var tbody = row.parent('tbody');
    		// Remove the old table row
    		row.remove();
    		// Append the new user to the table
			controllerPost(
				'system_view_return',
				{
					view: 'user/user',
					data: {
						uid: button.attr('data-uid'),
					},
				}, function(data) {
					tbody.append(data.buffer);
    			}
    		);
    	});
    	
    	// Close modal
		$(this).closest('.edit-user-modal').modal('hide');
	});


	// POPUP / ALERT

	// Remove popup when clicked elsewhere on the page
	$("html").on('click', function(){
		$(message).fadeOut("fast", function(){
			$(this).remove();
		});
	});
	$(messageContainer).on('click', function(e){
		e.stopPropagation();
	});
});

