<?php
  include_once(dirname(__FILE__) . '/../models/helper_functions.php');

  $limit = 3;

  // Get movie data from feed_movies.xml
  $feed_movies = new DOMDocument();
  $feed_movies->load(PLUGIN_URL . 'portal/feeds/feed_movies.xml');
  $feed = array();

  $server_url = $feed_movies->getElementsByTagName('serverUrl')->item(0)->nodeValue;
  $first_movie = $feed_movies->getElementsByTagName('item')->item(0);
  if (isset($first_movie)) {
    $movie_section_key = $feed_movies->getElementsByTagName('movieSectionKey')->item(0)->nodeValue;

    foreach ($feed_movies->getElementsByTagName('item') as $node) {
      $item = array ( 
        'key' => $node->getElementsByTagName('key')->item(0)->nodeValue,
        'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
        'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
        'thumb' => $node->getElementsByTagName('thumb')->item(0)->nodeValue,
        'imdb_url' => $node->getElementsByTagName('imdbUrl')->item(0)->nodeValue,
        'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
        'year' => $node->getElementsByTagName('year')->item(0)->nodeValue,
        'duration' => $node->getElementsByTagName('duration')->item(0)->nodeValue,
        );
      array_push($feed, $item);
    }
  }
?>
<div class="panel-heading">
  <h3 class="panel-title">Recently added movies</h3>
  <?php if (isset($movie_section_key) && visitor_is_local()): ?>
    <a href="<?=BASE_URL . 'plex?category=' . $movie_section_key?>" class="btn btn-default btn-xs more">more</a>
  <?php endif ?>
</div>
<div class="panel-body">
  <?php
    if (isset($first_movie)) {
      for ($x = 0; $x < $limit; $x++) {
        $title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
        $date = date('l F d, H:i', strtotime($feed[$x]['date']));
        $duration = floor((intval($feed[$x]['duration']) / 1000) / 60) . ' minutes';
        $key = $feed[$x]['key'];
        $description = $feed[$x]['desc'];
        $poster = $feed[$x]['thumb'];
        $imdb_url = $feed[$x]['imdb_url'];
        $year = $feed[$x]['year'];
  ?>
  <div class="item">
    <p>
      <a data-toggle="modal" data-target="#modal-movie-<?=$x?>" class="title">
        <strong><?=$title . ' (' . $year?>)</strong>
      </a><br />
      <small class="date"><em>Added on <?=$date?></em></small>
    </p>
    <p class="description">
      <?php if ($poster && visitor_is_local()): ?>
        <a data-toggle="modal" data-target="#modal-movie-<?=$x?>" class="title"><img class="poster" src="<?=$poster?>" /></a>
      <?php endif ?>
      <?=truncate($description, 250)?>
      <a class="read-more" data-toggle="modal" data-target="#modal-movie-<?=$x?>">[Read more]</a>
      <div class="clearfix"></div>
    </p>
    <div class="modal fade" id="modal-movie-<?=$x?>" tabindex="-1" role="dialog" aria-labelledby="movie" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?=$title . ' (' . $year?>)</h4>
          </div>
          <div class="modal-body">
            <?php if ($poster && visitor_is_local()): ?>
              <img class="poster" src="<?=$poster?>" />
            <?php endif ?>
            <div class="content-right">
              <p class="date"><strong>Added on</strong><br/><?=$date?></p>
              <p class="duration"><strong>Duration</strong><br/><?=$duration?></p>
              <p class="description"><strong>Plot summary</strong><br/><?=$description?></p>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a class="btn btn-primary imdb" href="<?=$imdb_url?>" target="_blank">View on IMDB</a>
            <?php if (visitor_is_local()): ?>
              <a class="btn btn-primary" href="<?=BASE_URL . 'plex?item=' . $key?>">View on Plex</a>
            <?php endif ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
      }
    } else {
      echo 'There are no movies added to plex.';
    }
  ?>
</div>
