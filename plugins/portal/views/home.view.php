<?php  
  // Get updated date from feed_shows.xml
  $feed = new DOMDocument();
  $feed->load(PLUGIN_URL . 'portal/feeds/feed_shows.xml');
  $updated = $feed->getElementsByTagName('documentUpdated')->item(0)->nodeValue;
  $updated = date('H:i', strtotime($updated));
?>
<span class="plex-updated">Lastly updated: <?=$updated?></span>
<div class="shortcuts panel panel-default">
	<div class="panel-heading">
   		<h3 class="panel-title">Shortcuts</h3>
  	</div>
  	<div class="panel-body">
		<div class="shortcut">
			<a href="<?=BASE_URL . 'bierlijst/home'?>" class="bierlijst">Bierlijst</a>
		</div>
		<div class="shortcut">
			<a href="<?=BASE_URL . 'plex'?>" class="plex" data-toggle="tooltip" data-placement="top" title="Watch media">Plex</a>
		</div>
		<div class="shortcut">
			<a href="<?=BASE_URL . 'couchpotato'?>" class="couchpotato" data-toggle="tooltip" data-placement="top" title="Download movies">CouchPotato</a>
		</div>
		<div class="shortcut">
			<a href="<?=BASE_URL . 'sickbeard'?>" class="sickbeard" data-toggle="tooltip" data-placement="top" title="Download series">Sickbeard</a>
		</div>
	</div>
</div>
<div class="feed series panel panel-default">
	<?php include("feed_shows.view.php"); ?>
</div>
<div class="feed movies panel panel-default">
	<?php include("feed_movies.view.php"); ?>
</div>