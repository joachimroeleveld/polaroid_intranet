<div class="shortcuts panel panel-default">
	<div class="panel-heading">
   		<h3 class="panel-title">Shortcuts</h3>
  	</div>
  	<div class="panel-body">
		<div class="shortcut">
			<a href="http://download.villapolaroid.eu/" class="download" target="_blank">Download server</a>
		</div>
		<div class="shortcut">
			<a href="http://monitoring.villapolaroid.eu/" class="monitoring" target="_blank">Network monitor</a>
		</div>
		<div class="shortcut">
			<a href="http://database.villapolaroid.eu/" class="database" target="_blank" data-toggle="tooltip" data-placement="top" title="Available only from LAN for security reasons">Database</a>
		</div>
	</div>
</div>
<div class="routers panel panel-default">
	<div class="panel-heading">
   		<h3 class="panel-title">Routers</h3>
  	</div>
  	<div class="panel-body">
  		<div class="house">
  			<a href="#" class="router floor2" data-toggle="modal" data-target="#floor2-modal">Vloer 2 - TP-Link</a>
  			<div class="modal fade" id="floor2-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">Router vloer 2</h4>
			      </div>
			      <div class="modal-body">
		      		<strong>IP:</strong> 192.168.1.1<br/>
		      		<strong>Model: </strong> TP-Link WR1043ND<br/>
		      		<strong>Wireless channel: </strong> 2<br/>
		      		<strong>Guest network enabled: </strong> yes
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      			<a href="http://router2.villapolaroid.eu/" target="_blank" type="button" class="btn btn-primary">Ga naar webinterface</a>
			      </div>
			    </div>
			  </div>
			</div>
  			<a href="#" class="router floor1" data-toggle="modal" data-target="#floor1-modal">Vloer 1 - Cisco</a>
  			<div class="modal fade" id="floor1-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">Router vloer 1</h4>
			      </div>
			      <div class="modal-body">
		      		<strong>IP:</strong> 192.168.1.3<br/>
		      		<strong>Model: </strong> Linksys E1000 v2<br/>
		      		<strong>Wireless channel: </strong> 7<br/>
		      		<strong>Guest network enabled: </strong> yes
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      			<a href="http://router3.villapolaroid.eu/" target="_blank" type="button" class="btn btn-primary">Ga naar webinterface</a>
			      </div>
			    </div>
			  </div>
			</div>
  			<a href="#" class="router floor0" data-toggle="modal" data-target="#floor0-modal">Begane grond - TP-Link</a>
			<div class="modal fade" id="floor0-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">Router begane grond</h4>
			      </div>
			      <div class="modal-body">
		      		<strong>IP:</strong> 192.168.1.2<br/>
		      		<strong>Model: </strong> TP-Link WR1043ND<br/>
		      		<strong>Wireless channel: </strong> 9<br/>
		      		<strong>Guest network enabled: </strong> yes
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      			<a href="http://router1.villapolaroid.eu/" target="_blank" type="button" class="btn btn-primary">Ga naar webinterface</a>
			      </div>
			    </div>
			  </div>
			</div>
  			<a href="#" class="router exterior" data-toggle="modal" data-target="#exterior-modal">Buiten - Belkin</a>
			<div class="modal fade" id="exterior-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">Router buiten</h4>
			      </div>
			      <div class="modal-body">
		      		<strong>IP:</strong> 192.168.1.4<br/>
		      		<strong>Model: </strong> Belkin F5D7230-4 v1444<br/>
		      		<strong>Wireless channel: </strong> 5<br/>
		      		<strong>Guest network enabled: </strong> no
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      			<a href="http://router4.villapolaroid.eu/" target="_blank" type="button" class="btn btn-primary">Ga naar webinterface</a>
			      </div>
			    </div>
			  </div>
			</div>
  		</div>
  	</div>
</div>