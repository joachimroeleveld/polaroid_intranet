<?php
  include_once(dirname(__FILE__) . '/../models/helper_functions.php');

  $limit = 6;

  // Get show data from feed_shows.xml
  $feed_shows = new DOMDocument();
  $feed_shows->load(PLUGIN_URL . 'portal/feeds/feed_shows.xml');
  $feed = array();

  $server_url = $feed_shows->getElementsByTagName('serverUrl')->item(0)->nodeValue;
  $first_show = $feed_shows->getElementsByTagName('item')->item(0);
  if (isset($first_show)) {
    $show_section_key = $feed_shows->getElementsByTagName('showSectionKey')->item(0)->nodeValue;

    foreach ($feed_shows->getElementsByTagName('item') as $node) {
      $item = array(
        'key' => $node->getElementsByTagName('key')->item(0)->nodeValue,
        'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
        'parentTitle' => $node->getElementsByTagName('parentTitle')->item(0)->nodeValue,
        'thumb' => $node->getElementsByTagName('thumb')->item(0)->nodeValue,
        'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
        'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
        'year' => $node->getElementsByTagName('year')->item(0)->nodeValue,
        'duration' => $node->getElementsByTagName('duration')->item(0)->nodeValue,
        'season' => $node->getElementsByTagName('parentIndex')->item(0)->nodeValue,
        'episode' => $node->getElementsByTagName('index')->item(0)->nodeValue,
        );
      array_push($feed, $item);
    }
  }
?>
<div class="panel-heading">
  <h3 class="panel-title">Recently added episodes</h3>
  <?php if (isset($show_section_key) && visitor_is_local()): ?>
    <a href="<?=BASE_URL . 'plex?category=' . $show_section_key?>" class="btn btn-default btn-xs more">more</a>
  <?php endif ?>
</div>
<div class="panel-body">
  <?php
    if(isset($first_show)) {
      for ($x = 0; $x < $limit; $x++) {
        $title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
        $date = date('l F d, H:i', strtotime($feed[$x]['date']));
        $duration = floor((intval($feed[$x]['duration']) / 1000) / 60) . ' minutes';
        $key = $feed[$x]['key'];
        $description = $feed[$x]['desc'];
        $poster = $feed[$x]['thumb'];    
        $year = $feed[$x]['year'];
        $serie = $feed[$x]['parentTitle'];
        $season = $feed[$x]['season'];
        $episode = $feed[$x]['episode'];
  ?>
  <div class="item">
    <p>
      <a data-toggle="modal" data-target="#modal-show-<?=$x?>" class="title">
        <strong>Season <?=$season . ', Episode ' . $episode . ' - ' . $serie?></strong>
      </a><br />
      <small class="date"><em>Added on <?=$date?></em></small>
    </p>
    <p class="description">
      <?php if ($poster && visitor_is_local()): ?>
        <a data-toggle="modal" data-target="#modal-show-<?=$x?>" class="title"><img class="poster" src="<?=$poster?>" /></a>
      <?php endif ?>
      <?=truncate($description, 95)?>
      <a class="read-more" data-toggle="modal" data-target="#modal-show-<?=$x?>">[Read more]</a>
    </p>
    <div class="modal fade" id="modal-show-<?=$x?>" tabindex="-1" role="dialog" aria-labelledby="show" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Season <?=$season . ', Episode ' . $episode . ' - ' . $serie?></h4>
          </div>
          <div class="modal-body">
            <?php if ($poster && visitor_is_local()): ?>
              <img class="poster" src="<?=$poster?>" />
            <?php endif ?>
            <div class="content-right">
              <p class="date"><strong>Added on</strong><br/><?=$date?></p>
              <p class="year"><strong>Title</strong><br/><?=$title?></p>
              <p class="duration"><strong>Duration</strong><br/><?=$duration?></p>
              <p class="description"><strong>Plot summary</strong><br/><?=$description?></p>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <?php if (visitor_is_local()): ?>
              <a class="btn btn-primary" href="<?=BASE_URL . 'plex?item=' . $key?>">View on Plex</a>
            <?php endif ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
      }
    } else {
      echo 'There are no shows added to Plex.';
    }
  ?>
</div>
