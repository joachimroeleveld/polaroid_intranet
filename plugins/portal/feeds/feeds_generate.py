#!/Python27/python -u
# That "-u" is important when running on Windows, it sets std.out (used by print) to binary mode
import cgi, urllib, sys, getopt
from pprint import pprint
from xml.dom import minidom
from time import gmtime, strftime
from string import split

usage = 'Usage: feeds_generate.py -c [movie|show|artist]'

# Read command line arguments
if __name__ == "__main__":
  try:
    opts, args = getopt.getopt(sys.argv[1:],"hc:",["category="])
  except getopt.GetoptError:
      print usage
      sys.exit(2)
  for opt, arg in opts:
    if opt == '-h':
      print usage
      sys.exit()
    elif opt in ("-c", "--category"):
      category = arg

try:
  category
except NameError:
  print usage
else:
  if category == 'movie' or category == 'show' or category == 'artist':
    host = '192.168.2.99' # location of PMS relative to this script
    port = '32400' # port the PMS web service runs on (default 32400)
    source_type = [category] # section type to filter, 'movie' for movies, 'show' for TV shows, 'artist' for music sections
    max_entries = 6 # maximum number of entries

    base_url = 'http://' + host + ':' + port
    web_base_url = base_url + '/web/index.html#!'
    server_url = base_url + '/servers'
    library_url = base_url + '/library/'
    sections_url = library_url + 'sections'
    recently_added_url = sections_url + '/{}/recentlyAdded'
    metadata_url = library_url + 'metadata/'
    
    # get the server machine identifier and create the server url from it
    try:
      xml_servers = minidom.parse(urllib.urlopen(server_url))
      servers = xml_servers.getElementsByTagName('Server')
      machineId = servers[0].getAttribute('machineIdentifier')
      server_web_url = web_base_url + '/server/' + machineId
    except:
      pass

    print '<?xml version="1.0" encoding="utf-8"?>'
    print '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">'
    print '  <channel>'
    print '    <title>Recently Added</title>'
    print '    <description>Media items recently added to the Plex library</description>'
    print '    <link>http://www.plex.tv</link>'
    print '    <documentUpdated>' + strftime('%a, %d %b %Y %H:%M:%S GMT', gmtime()) + '</documentUpdated>'
    print '    <serverUrl>' + server_web_url + '</serverUrl>'
    # create tags for section keys
    try:
      xml_sections = minidom.parse(urllib.urlopen(sections_url))
      sections = xml_sections.getElementsByTagName('Directory')
      for section in sections:
        print '    <' + section.getAttribute('type') + 'SectionKey>' + section.getAttribute('key') + '</' + section.getAttribute('type') + 'SectionKey>'
    except:
      pass

    items = []
    ratingKey_list = ''
    items_count = 0
    try:
      xml_sections = minidom.parse(urllib.urlopen(sections_url))
      sections = xml_sections.getElementsByTagName('Directory')
      for s in sections:
        if items_count >= max_entries:
          break
        type = s.getAttribute('type')
        if type in source_type:
          key = s.getAttribute('key')
          # Get the list of recently added content for this section
          try:
            xml_media = minidom.parse(urllib.urlopen(recently_added_url.format(key)))
            media = xml_media.getElementsByTagName('Video')
            for m in media:
              if items_count >= max_entries:
                break
              ratingKey = int(m.getAttribute('ratingKey'))
              if ratingKey_list:
                ratingKey_list = ratingKey_list + ',' + str(ratingKey)
              else:
                ratingKey_list = str(ratingKey)
              items_count += 1
          except:
            pass
          
          # Get the necessary meta data for the collection of ratingKeys
          try:
            xml_metadata = minidom.parse(urllib.urlopen(metadata_url + ratingKey_list))
            metadata = xml_metadata.getElementsByTagName('Video')
            for m in metadata:
              items.append([m.getAttribute('ratingKey'), m.getAttribute('title'), m.getAttribute('summary'), m.getAttribute('guid').split('//')[1].split('?')[0], int(m.getAttribute('addedAt')), m.getAttribute('grandparentTitle'), base_url + m.getAttribute('thumb'), m.getAttribute('duration'), m.getAttribute('year'), m.getAttribute('index'), m.getAttribute('parentIndex')])
          except:
            pass
      
      # Now output all of it
      for i in items:
        title = i[1].encode('utf8')
        print '    <item>'
        print '      <key>' + i[0] + '</key>'
        print '      <title>' + title + '</title>'
        if category == 'show' : print '      <parentTitle>' + i[5] + '</parentTitle>'
        if category == 'show' : print '      <index>' + i[9] + '</index>'
        if category == 'show' : print '      <parentIndex>' + i[10] + '</parentIndex>'
        if category == 'movie' : print '      <imdbUrl>http://www.imdb.com/title/' + i[3] + '/</imdbUrl>'
        print '      <description>'
        print '        <![CDATA[' + i[2].encode('utf8') + ']]>'
        print '      </description>'
        print '      <thumb>' + i[6] + '</thumb>'
        print '      <duration>' + i[7] + '</duration>'
        print '      <year>' + i[8] + '</year>'
        print i[4]
        print '      <pubDate>' + strftime('%a, %d %b %Y %H:%M:%S GMT', gmtime(i[4])) + '</pubDate>'
        print '    </item>'
    except:
      pass

    print '  </channel>'
    print '</rss>'
  else:
    print usage
