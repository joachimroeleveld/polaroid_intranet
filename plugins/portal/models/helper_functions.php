<?php
	include_once(MODEL_PATH . 'template_functions.php');

	function truncate($string, $length, $dots = "...") {
    	return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
	}