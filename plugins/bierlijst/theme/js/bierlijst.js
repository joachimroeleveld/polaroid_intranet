// (GLOBAL) VARIABLES

var plugin_name = 'bierlijst';
var plugin_paths = {
    'theme' : getPluginPath(plugin_name) + 'theme/',
    'controllers' : getPluginPath(plugin_name) + 'controllers.php',
    'views' : getPluginPath(plugin_name) + 'views/',
    'profile_pictures' : baseUrl + 'files/images/profile_pictures/'
};
// Max size for profile pictures (in MB)
var max_allowed_upload_size = 2;
var actions = ["increment", "decrement"];


// SELECTOR VARIABLES

var messageContainer = '#message-container';


//GLOBAL FUNCTIONS

function updateAmountReport() {
	var form = $('.adminsection .panel#reports .from_until');
	var from = form.find('#date_from').val();
	var to = form.find('#date_until').val();

	controllerPost(
		'get_reports_view',
		{
			between: {
				from: from,
				to: to
			},
		}, function(data) {
			form.closest('.panel-body').find('#report_amount').html(data.buffer).hide().fadeIn("fast");
		},
		plugin_paths['controllers']
	);
}

function changeAmount(argAction, uid, successCallback) {
	if ($.inArray(argAction, actions) > -1) {
		var functionToCall = "add_beer";
		if (argAction != 'increment') {
			functionToCall = "remove_beer";
		}

		controllerPost(
			functionToCall,
			{ uid: uid },
			successCallback,
			plugin_paths['controllers']
		);
	}
}

function resizeDrinkers() {
	setTimeout(function(){
	    $('#drinkers .drinker').each(function(){
		    var profile_picture = $(this).find('.profile_picture')
		    profile_picture.css({
		        'height': profile_picture.outerWidth()
		    });
	    });
   	}, 100);
}


// ON WINDOW RESIZE

$(window).resize(function() {
	resizeDrinkers();
});


// WHEN DOCUMENT IS READY

$(document).ready(function() {
	// CALLS TO CLOBAL FUNCTIONS

	if ($('body').is('.bierlijst')) {
		resizeDrinkers();
	}
	if ($('body').is('.adminsection')) {
		updateAmountReport();
	}

	// MISC

	$('input[data-role="datepicker"]').each(function(){
		$(this).datepicker({
		    format: "dd-mm-yyyy",
		    endDate: "now",
		    autoclose: true,
		    todayHighlight: true,
		    todayBtn: "linked"
		});
	});

	// ADMIN SECTION BINDINGS

	$('body.adminsection #general_actions').on('click', '.clear-records button', function(){
		BootstrapDialog.confirm('Are you sure you want to clear ALL records? This action cannot be undone.', function(result){
            if(result) {
				controllerPost('delete_records', null, function(){
					updateAmountReport();
					$('#adjust tr[uid]').find('.amount').each(function(){
						$(this).text('0');
					});
				}, plugin_paths['controllers']);
            }
        });
	});

	$('body.adminsection #reports').on('change', '#date_from, #date_until', function(){
		updateAmountReport();
	});

	$('body.adminsection #adjust').on('click', '.adjustments button', function(){
		var thiss = $(this);
		var action = thiss.attr('data-action');

		changeAmount(
			action, 
			thiss.closest('tr').attr('uid'),
			function() {
				var amount = thiss.closest('tr').find('.amount');
				var currentValue = parseInt(amount.text());

				if (action == 'increment') {
					amount.text(currentValue + 1);
				} else {
					amount.text(currentValue - 1);
				}
				updateAmountReport();
			}
		);
	});

	// PREFERENCES SECTION BINDINGS

	$('body.preferences').on('click', '.actions .add_user', function(){
		var panel = $(this).closest('.panel.drinkers');
		var drinker = panel.find('.drinker.add');
		var addNewHTML = drinker[0].outerHTML;
		var inputGroup = drinker.find('.save-name');

		if (!drinker.is(':visible')) {
			drinker.fadeIn("fast");
			panel.addClass('add');

			$(drinker).on('click', '.save-name .save', function() {
				var newName = inputGroup.find('input').val();

				controllerPost(
					'add_drinker',
					{ name: newName }, 
					function(data) {
						var name = drinker.find(".name");
						panel.removeClass('add');

						inputGroup.fadeOut("fast", function(){
							name.fadeIn("fast");
							name.find(".nameText").text(newName);
							drinker.find(".profile_picture").fadeIn("fast");
							$(this).remove();
						});
						drinker.removeClass('add');
						drinker.attr('uid', data.uid);
						
						$('.panel-body .actions').after(addNewHTML);
					},
					plugin_paths['controllers']
				);
			});
		} else {
			panel.removeClass('add');
			drinker.fadeOut("fast");
			$(this).blur();
		}
	});

	$('body.preferences').on('click', '.actions .remove_user', function(){
		var button = $(this);

		$(this).closest('.panel-body').find('.drinker').each(function(){
			if ($(this).hasClass('add')) {
				$(this).fadeOut();
			}
			var icon = $(this).find('.name .remove');
			var visible = icon.is(':visible');
			icon.fadeToggle();
			if (!visible) {
				$(button).addClass('active');
			} else {
				$(button).removeClass('active');
			}
		});
	});

	$('body.preferences').on('click', '.drinker .name .edit', function(){
		var parent = $(this).closest('.drinker');
		var currentInput = parent.find('.edit-name');
		var profile_picture = parent.find('.profile_picture');
		var nameObject = $(this);

		if (currentInput.length == 0) {
			profile_picture.fadeOut("fast", function(){
				$(nameObject).after('\
				<div class="input-group edit-name">\
				  <input type="text" class="form-control" placeholder="New name">\
				  <span class="input-group-btn">\
				    <button class="btn btn-default save" type="button">Save</button>\
				  </span>\
				</div>');
				$(nameObject).next().hide().fadeIn();
			});
		} else {
			currentInput.fadeOut("fast", function(){
				profile_picture.fadeIn("fast");
				$(this).remove();
			});
		}
	});

	$('body.preferences').on('click', '.drinker .edit-name .save', function(){
		var input = $(this).parent().prev();
		var newName = input.val();
		var uid = $(this).closest('.drinker').attr('uid');

		if (newName.length != 0) {
			controllerPost(
				'edit_name',
				{
					uid: uid,
					name: newName
				},
				function(data) {
					var drinker = input.closest('.drinker');
					drinker.find('.nameText').text(newName);
					input.parent('.input-group').fadeOut("fast", function(){
						drinker.find('.profile_picture').fadeIn("fast");
						$(this).remove();
					});
				},
				plugin_paths['controllers']
			);
		}
	});

	$('body.preferences').on('click', '.drinker .name .remove', function(){
		var drinker = $(this).closest('.drinker');
		var uid = drinker.attr('uid');

		controllerPost(
			'remove_drinker',
			{ uid: uid }, 
			function() {
				drinker.fadeOut("fast", function() {
					$(this).remove();
				});
			},
			plugin_paths['controllers']
		);
	});

	$('body.preferences').on('change', '.drinker .profile_picture .change input, .drinker .profile_picture .upload input', function(){
		event.stopPropagation(); // Stop stuff happening
	    event.preventDefault(); // Totally stop stuff happening

	    var oldExists = '';
	    if ($(this).attr('name') == 'profile_picture_change') {
	    	oldExists = 'oldExists';
	    }

	    // Create a formdata object and add the files
		var formData = new FormData($(this).parent('form')[0]);

		var drinker = $(this).closest('.drinker');
		var profile_picture = drinker.find('.profile_picture');
		var uid = drinker.attr('uid');

		// If bigger than 2 MB
		if ((this.files[0].size / 1000000) > max_allowed_upload_size) {
			alertMessage("File too big. Max is " + max_allowed_upload_size + " MB", "danger");
			return;
		}
	    
	    $.ajax({
	        type: 'POST',
	        url: plugin_paths['controllers'] + '?file=' + uid + '&' + oldExists,
	        data: formData,
	        cache: false,
	        dataType: dataType,
	        contentType: false,
	        processData: false,
	        beforeSend: function() {
	        	profile_picture.append('<div class="loading"></div>');
	        },
	        success: function(data, textStatus, jqXHR) {
	        	postSucces(data, textStatus, jqXHR, 'upload_profile_picture', function(data) {
					var newSrc = plugin_paths['profile_pictures'] + 'profile_picture_thumb/' + data.file;
	        		if (oldExists) {
	        			profile_picture.children('img').attr("src", newSrc);
	        		} else {
	        			profile_picture.append('<img src="' + newSrc + '" />');
	        		}
	        	});
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	        	postFail(jqXHR, textStatus, errorThrown);
	        },
	        complete: function() {
	        	profile_picture.find('.loading').remove();
	        	if (!oldExists) {
	        		var btn = drinker.find('.btn.upload');
	        		btn
	        			.addClass('change')
	        			.removeClass('upload')
	        			.find('input').attr('name', 'profile_picture_change');
	        		btn.find('span').text('Change');
	        	}
	        }
	    });
	});

	// AJAX BEER ADDING/REMOVING PROCESSING

	$('#drinkers').on('click', '.drinker', function(){
		var id = $(this).attr('uid');
		var action = "increment";

		changeAmount(
			action,
			id,
			function() {
				changeAmountVisually(action, id);
			}
		);
	});
	$(messageContainer).on('click', '.undo', function(){
		var id = $(this).attr('uid');
		var action = "decrement";

		changeAmount(
			action,
			id,
			function() {
				changeAmountVisually(action, id);
			}
		);
	});

	function changeAmountVisually(argAction, uid) {
		if ($.inArray(argAction, actions) > -1) {
			var drinker = $('#drinkers .drinker[uid="' + uid + '"]').find('.amount');
			var amount = parseInt(drinker.text());

			if (argAction == 'increment') {
				drinker.text(amount + 1);
			} else {
				drinker.text(amount - 1);
			}

			actions.forEach(function(action){
				drinker
					.removeClass(action)
					.removeClass(action + 'ed')
			});

			drinker.addClass(argAction);
			setTimeout(function(){
				drinker.addClass(argAction + 'ed');
			}, 100);

			setKing();
		}
	}	

	function setKing() {
		controllerPost('parse_king', null, function(data){
			king = data.king;

			if (king != '-1') {
				var currentKing = $('#drinkers .drinker.king');
				var currentKingExists = $(currentKing).length != 0;
				if (currentKingExists) {
					var currentMax = parseInt(currentKing.find('.amount').text());
				}
				var newDrinker = $('#drinkers .drinker[uid="' + king + '"]');
				var newAmount = parseInt(newDrinker.find('.amount').text());

				function setNewCrown() {
					newDrinker
						.append('<img src="' + plugin_paths['theme'] + 'images/king.png" class="crown" />')
						.addClass('king');
					newDrinker.find('.crown')
						.hide()
						.fadeIn(1000);
				}

				if (currentKing.attr('uid') != king) {
					if (currentKingExists) {
						if (newAmount > currentMax) {
							currentKing
								.removeClass('king')
								.find('.crown').remove();
							setNewCrown();
						}
					} else {
						setNewCrown();
					}
				}
			}
		}, plugin_paths['controllers']);
	}
});

