<?php
	defined('SITE_ROOT') or
		define('SITE_ROOT', dirname(__FILE__) . '/../../');

	require_once SITE_ROOT . 'settings.php';
	require_once SITE_ROOT . 'system/models/includes/constants.inc';
	require_once SITE_ROOT . 'system/models/Database.php';
	require_once 'models/template_functions.php';
	require_once 'models/images/image_styles.php';

	$min_profile_picture_size = 250; // (width, height, unit: px)

	require_once SITE_ROOT . 'system/controllers.php';
	if(isset($_GET['file'])) {
		upload_profile_picture();
	}

	function get_reports_view($data) {
		html_return($data, dirname(__FILE__) . '/views/reports.view.php');
	}

	function amount_report_to_csv() {
		// Get the amounts per drinker and delete the drinker's 
		// id since this is not relevant
		$amount_per_drinker = get_amount_per_drinker();
		foreach ($amount_per_drinker as $drinker) {
			unset($drinker['id']);
			$to_csv[] = $drinker;
		}

		// Set download headers
		$filename = "amount_per_drinker.csv";
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename='.$filename);
	    header('Pragma: no-cache');
	    header('Expires: 0');
		// Print CSV rows
		foreach($to_csv as $row) {
		    echocsv($row);
		}
		exit;
	}

	function add_beer($data) {
		$uid = $data['uid'];
		$date = date('Y-m-d H:i:s');

		$row    = array (
		       'date'     => $date,
		       'drinker'  => $uid
		);
		$query = Database::get_manager()->insert($row, 'drink_record');

		if(!isset($query['error'])) {
			$drinker = Database::get_manager()->select_quick(array('id' => $uid), 'drinker');
		 	successful_return('<span class="beer_added">Beer added for ' . $drinker[0]['name'] . ' <a href="#" class="undo alert-link" uid="' . $uid . '">Undo</a></span>');
		} else {
			error_return('Failed to add the record to the database.');
		}
	}

	function remove_beer($data) {
		$uid = $data['uid'];

		foreach (get_amount_per_drinker() as $drinker) {
			if ($drinker['id'] == $uid && $drinker['amount'] == 0)
				error_return('Negative amounts of beer cannot be drunk.');
		}

		$sql    = "DELETE FROM drink_record WHERE drinker=? ORDER BY date DESC LIMIT 1";
		$params = array($uid);
		$query  = Database::get_manager()->generic($sql, $params);

		if(!isset($query['error'])) {
			$drinker = Database::get_manager()->select_quick(array('id' => $uid), 'drinker');
		 	successful_return('Beer removed for ' . $drinker[0]['name'] . '.');
		} else {
			error_return('Couldn\'t remove the record.');
		}
	}

	function parse_king($data) {
		successful_return(NULL, NULL, array('king' => get_king()));
	}

	function delete_records() {
		$query = Database::get_manager()->generic('DELETE FROM drink_record');

		if(!isset($query['error']))
			successful_return(array('Records removed successfully'));
		else
			error_return('Failed to remove records from the \'drink_record\' table');
	}

	function edit_name($data) {
		$query = Database::get_manager()->update_quick(array('name' => $data['name']), array('id' => $data['uid']), 'drinker');

		if(!isset($query['error']))
			successful_return(array('Name changed successfully'));
		else
			error_return('Failed to rename drinker');
	}

	function add_drinker($data) {
		$returnData = array();
		$messages = array();
		$name = $data['name'];
		
		$query = Database::get_manager()->insert(array('name' => $name), 'drinker');
		if(!isset($query['error'])) {
			$drinker = Database::get_manager()->select_quick(array('name' => $name), 'drinker');
			$returnData['uid'] = $drinker[0]['id'];
			$messages[] = 'Drinker added successfully';
		} else {
			error_return('Cannot add the drinker to the database. Propably there already exists a drinker with this name.');
		}

		successful_return($messages, NULL, $returnData);
	}

	function remove_drinker($data) {
		$messages = array();
		$warnings = array();
		$errors = array();

		$uid = $data['uid'];
		$drinker = Database::get_manager()->select_quick(array('id' => $uid), 'drinker');
		$profile_picture = $drinker[0]['profile_picture'];
		$dir = FILE_PATH . 'images/profile_pictures/';

		if(isset($profile_picture)) {
			$delete_images_error = delete_images($dir, $profile_picture, NULL);
			if(isset($delete_images_error))
				$warnings = 'Could not delete profile picture. ';
		}

		$query  = Database::get_manager()->delete_quick(array('drinker' => $uid), 'drink_record');
		$query2 = Database::get_manager()->delete_quick(array('id' => $uid), 'drinker');

		if(!isset($query['error']) && !isset($query2['error']))
			$messages[] = 'Deleted drinker successfully from the database.';
		else
			error_return('Failed to delete drinker');

		successful_return($messages, $warnings);
	}

	function upload_profile_picture() {
		global $image_styles;
		global $min_profile_picture_size;
		$oldExists = isset($_GET['oldExists']);
    	$styles = array($image_styles['profile_picture_thumb'], $image_styles['profile_picture_front']);
		$uid = $_GET['file'];
		// Return values
		$messages = array();
		$warnings = array();
		$data = array();

		if ($oldExists)
			$file = $_FILES['profile_picture_change'];
		else 
			$file = $_FILES['profile_picture_upload'];
		$filename = $file['name'];
		$dir = FILE_PATH . 'images/profile_pictures/';

		list($width, $height) = getimagesize($file['tmp_name']);
		if ($width < $min_profile_picture_size || $height < $min_profile_picture_size)
			error_return('The image is too small. The required minimum size is ' . $min_profile_picture_size . 'x' . $min_profile_picture_size . '.');

		if ($oldExists) {
			$drinker = Database::get_manager()->select_quick(array('id' => $uid), 'drinker');
			$currentPic = $drinker[0]['profile_picture'];
			$delete_error = delete_images($dir, $currentPic, $styles);
			if (isset($delete_error))
				$warnings[] = $delete_error;
		}

		$upload = upload_image($file, $dir);
		if (isset($upload['error']))
			error_return($upload['error']);
		else
			$filename = $upload['filename'];
			
	    create_image_styles($dir, $filename, $styles);

		$query = Database::get_manager()->update_quick(array('profile_picture' => $filename), array('id' => $uid), 'drinker');

		if(isset($query['error']))
			error_return('An error occured while updating the profile picture in the database');

		$messages[] = 'Successfully uploaded the profile picture.';
		$data['file'] = $filename;

		successful_return($messages, $warnings, $data);
	}

