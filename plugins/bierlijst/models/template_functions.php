<?php
	require_once 'images/image_styles.php';

	function get_profile_pic_url($uid, $style = '') {
		$drinker = Database::get_manager()->select_quick(array('id' => $uid), 'drinker');
		$drinker = $drinker[0];
		$profile_picture = $drinker['profile_picture'];

		if (isset($profile_picture)) {
			return FILE_URL . 'images/profile_pictures/' . (($style != '') ? ($style . '/') : '') . $drinker['profile_picture'] . '" alt="' . $drinker['name'];
		} else {
			return false;
		}
	}

	function get_king() {
		$sql    = 'SELECT drinker, count(drinker) AS amount FROM drink_record GROUP BY drinker ORDER BY amount DESC LIMIT 1';
		$result = Database::get_manager()->select($sql);

		if (isset($result[0])) {
			return $result[0]['drinker'];
		}

		return '-1';
	}

	/**
	 * Check $_FILES[][name]
	 * @param (string) $filename - Uploaded file name.
	 */
	function check_file_uploaded_name($filename)
	{
	    (bool) ((preg_match("`^[-0-9A-Z_\.]+$`i",$filename)) ? true : false);
	}

	/**
	 * Check $_FILES[][name] length.
	 * @param (string) $filename - Uploaded file name.
	 */
	function check_file_uploaded_length($filename)
	{
	    return (bool) ((mb_strlen($filename,"UTF-8") > 225) ? true : false);
	}

	/**
	 * Check if file already exists in directory and renames it to a non-existent filename if it does.
	 * @param (string) $filename - Uploaded file name.
	 * @param (string) $dir - Directory of file.
	 * @return Unique filename for the passed file in the passed directory
	 */
	function check_file_path($dir, $filename) {
		if (file_exists($dir . $filename)) {
			$actual_name = pathinfo($filename, PATHINFO_FILENAME);
			$original_name = $actual_name;
			$extension = pathinfo($filename, PATHINFO_EXTENSION);

			$i = 1;
			while(file_exists($dir . $actual_name . "." . $extension)) {           
			    $actual_name = (string) $original_name . $i;
			    $i++;
			}

			return $actual_name . "." . $extension;
		}
		
		return $filename;
	}

	function upload_image($file_object, $directory, $allowed_extensions = array('gif','jpg','jpeg','png')) {
		$filename = $file_object['name'];
		$path = $directory . basename($filename);

		if(check_file_uploaded_name($filename))
			return array('error' => 'Not a valid filename');

		if(check_file_uploaded_length($filename))
			return array('error' => 'File name too long');

		$extensionsString = '';
		if(!in_array(pathinfo($filename, PATHINFO_EXTENSION), $allowed_extensions)) {
			foreach ($allowed_extensions as $ext) {
				$extensionsString .= ' .' . $ext;
			}
			return array('error' => 'Not a valid image extension (you can upload:' . $extensionsString . ')');
		}

		$filename = check_file_path($directory, $filename);
		$path = $directory . $filename;

		if(!move_uploaded_file($file_object['tmp_name'], $path))
		    return array('error' => 'An error occured while moving the file');

		return array('filename' => $filename);
	}

	function delete_file($file) {
	  	if (file_exists($file) && unlink($file)) {
	    	return NULL;
	    } else {
	    	return 'Could not delete file or it does not exist';
	    }
	}

	function create_image_styles($dir, $file, $styles) {
		global $image_styles;
		if (!isset($styles))
			$styles = $image_styles;

		foreach ($image_styles as $image_style) {
			$style_dir = $dir . $image_style->getName();
			if (!file_exists($style_dir))
				mkdir($style_dir, 0777, true);

			$image_style->generate($dir . $file, $style_dir . '/' . $file);
		}
	}

	function delete_images($dir, $file, $image_styles) {
		global $image_styles;
		if (!isset($styles))
			$styles = $image_styles;

		$error = delete_file($dir . $file);
		foreach ($image_styles as $image_style) {
			$error_styles = delete_file($dir . $image_style->getName() . '/' . $file);
			$error = ((!isset($error) && !isset($error_styles)) ? $error_styles : NULL);
		}

		return $error;
	}

	function get_amount_per_drinker($between = NULL) {
		if (isset($between))
			$from_date = $between['from'];
			$to_date = $between['to'];

		$sql = '
			SELECT drinker.id, name, count(drink_record.id) AS amount FROM drink_record 
			RIGHT JOIN drinker ON drinker.id = drink_record.drinker' .
			(isset($between) ? (' WHERE (date BETWEEN STR_TO_DATE(\'' . date("d-m-Y", strtotime($from_date . " -1 day")) . '\', \'%d-%m-%Y\') AND STR_TO_DATE(\'' . date("d-m-Y", strtotime($to_date . " +1 day")) . '\', \'%d-%m-%Y\'))' ) : '')
			. ' GROUP BY name 
			ORDER BY name ASC
		';
		return Database::get_manager()->select($sql);
	}

	/**
	 * Echoes the input array as csv data maintaining 
	 * consistency with most CSV implementations
	 * - uses double-quotes as enclosure when necessary
	 * - uses double double-quotes to escape double-quotes 
	 * - uses CRLF as a line separator
	 */
	function echocsv($fields) {
	    $separator = '';
	    foreach ($fields as $field) {
	        if (preg_match('/\\r|\\n|,|"/', $field)) {
	            $field = '"' . str_replace('"', '""', $field) . '"';
	        }
	        echo $separator . $field;
	        $separator = ',';
	    }
	    echo "\r\n";
	}

