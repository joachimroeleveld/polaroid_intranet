<?php
	require_once 'operations.php';

	class Operation {
		private $name;
		private $properties = array();
		private $function;

		function __construct($name, $properties = array(), $function = NULL) {
			$this->name = $name;
			$this->properties = $properties;
			$this->function = $function;
		}

		function addProperty($property) {
			$this->properties[] = $property;
		}

		function setFunction($function) {
			$this->function = $function;
		}

		function generate($values, $file, $destination) {
			if (isset($this->function)) {
				call_user_func_array($this->function, array($values, $file, $destination));
			}
		}

		function getName() {
			return $this->name;	
		}

		function getProperties() {
			return $this->properties;	
		}

		function getFunction() {
			return $this->function;
		}
	}