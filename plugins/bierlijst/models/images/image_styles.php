<?php
    require_once 'ImageStyle.php';
    require_once 'Operation.php';

    // OPERATIONS
    $operations = array(
        'resize_and_crop' => new Operation(
            'resize_crop',
            array('width', 'height'),
            'resize_image'
        )
    );

    // IMAGE STYLES

    $image_styles = array(
        'profile_picture_thumb' => new ImageStyle(
            'profile_picture_thumb',
            array($operations['resize_and_crop']),
            array(
                'resize_crop' => array(
                    'width' => 90,
                    'height' => 90
                )
            )
        ),
        'profile_picture_front' => new ImageStyle(
            'profile_picture_front',
            array($operations['resize_and_crop']),
            array(
                'resize_crop' => array(
                    'width' => 250,
                    'height' => 250
                )
            )
        )
    );