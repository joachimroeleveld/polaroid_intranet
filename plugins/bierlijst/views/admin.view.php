<?php
	require_once MODEL_PATH . 'Database.php';
	include_once dirname(__FILE__) . '/../models/template_functions.php';
?>
<div class="panel-group" id="accordion">
  <div class="panel panel-default" id="general_actions">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#general_actions_collapsible">
      <h3 class="panel-title">General actions</h3>
    </div>
    <div class="panel-collapse collapse in" id="general_actions_collapsible">
      <div class="panel-body">
        <div class="clear-records">Clear database records: <button type="button" class="btn btn-default clear-records">Clear records</button></div>
      </div>
    </div>
  </div>
  <div class="panel panel-default" id="reports">
    <div class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" href="#reports_collapsible">
      <h3 class="panel-title">Reports</h3>
    </div>
    <div class="panel-collapse collapse" id="reports_collapsible">
      <div class="panel-body">
        <form class="form-inline from_until" role="form">
          <div class="form-group">
            From:&nbsp;
            <label class="sr-only" for="date_from">From</label>
          <input type="text" class="form-control" data-role="datepicker" id="date_from" placeholder="dd-mm-yyyy" value="<?=date("d-m-Y", strtotime("-3 months"));?>">
          </div>
          <div class="form-group">
            &nbsp;Until:&nbsp;
            <label class="sr-only" for="date_until">Until</label>
          <input type="text" class="form-control" data-role="datepicker" id="date_until" placeholder="dd-mm-yyyy" value="<?=date("d-m-Y");?>">
          </div>
        </form>
        <form method="post" action="<?=PLUGIN_URL?>bierlijst/controllers.php" class="to-csv">
          <button type="submit" class="btn btn-default">
            <i class="glyphicon glyphicon-list-alt"></i> Export to CSV
          </button>
          <input type="hidden" name="function" value="amount_report_to_csv">
        </form>
        <div class="clearfix"></div>
        <div id="report_amount"></div>
      </div>
    </div>
  </div>
  <div class="panel panel-default" id="adjust">
    <div class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" href="#adjust_collapsible">
      <h3 class="panel-title">Manual adjust</h3>
    </div>
    <div class="panel-collapse collapse" id="adjust_collapsible">
      <div class="panel-body">
        <table class="table">
          <thead>
            <th>Drinker</th>
            <th>Amount</th>
            <th>Actions</th>
          </thead>
          <tbody>
            <?php foreach (get_amount_per_drinker() as $drinker): ?>
              <tr uid="<?=$drinker['id'];?>">
                <td><?=$drinker['name'];?></td>
                <td class="amount"><?=$drinker['amount'];?></td>
                <td class="adjustments">
    				<button type="button" class="btn btn-default btn-xs" data-action="increment">
    					<span class="glyphicon glyphicon-chevron-up increment"></span>
    					Increment
    				</button>
    				<button type="button" class="btn btn-default btn-xs" data-action="decrement">
                  		<span class="glyphicon glyphicon-chevron-down decrement"></span>
                  		Decrement
                  	</button>
                </td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>