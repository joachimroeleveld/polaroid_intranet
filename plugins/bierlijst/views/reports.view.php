<?php
	if(isset($_POST['between'])) {
		$between = array(
			'from' => $_POST['between']['from'],
			'to' => $_POST['between']['to']
		);

		if (!DateTime::createFromFormat('d-m-Y', $between['from']) || !DateTime::createFromFormat('d-m-Y', $between['to'])) {
			echo('The input is not valid. Provide a date in the format dd-mm-yyyy.');
			return;
		}
	} else {
		return;
	}

	$drinkers = get_amount_per_drinker($between);
	if(!isset($drinkers)) {
		echo 'There are not yet any drink records.';
		return;
	}
?>
<div class="table-responsive">
	<table class="table">
		<thead>
			<th>Drinker</th>
			<th>Amount</th>
		</thead>
		<tbody>
			<?php foreach ($drinkers as $drinker): ?>
				<tr>
					<td><?=$drinker['name'];?></td>
					<td><?=$drinker['amount'];?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>