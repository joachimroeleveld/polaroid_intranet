<?php
	require_once MODEL_PATH . 'Database.php';
	include_once dirname(__FILE__) . '/../models/template_functions.php';
?>
<div id="drinkers">
	<?php foreach (get_amount_per_drinker() as $drinker): ?>
		<?php $king = get_king(); ?>
		<div class="drinker<?=($king == $drinker['id'] ? ' king' : '');?>" uid="<?=$drinker['id'];?>">
			<div class="profile_picture">
				<?php if ($king == $drinker['id']): ?>
					<img src="<?=PLUGIN_URL?>bierlijst/theme/images/king.png" class="crown" />
				<?php endif ?>
				<?php if (get_profile_pic_url($drinker['id'])): ?>
					<img src="<?=get_profile_pic_url($drinker['id'], 'profile_picture_front');?>" alt="<?=$drinker['name'];?>" />
				<?php else: ?>
					<span class="name"><?=$drinker['name'];?></span>
				<?php endif ?>
			</div>
			<div class="amount"><?=$drinker['amount'];?></div>
			<div class="clearfix"></div>
		</div>
	<?php endforeach ?>
</div>