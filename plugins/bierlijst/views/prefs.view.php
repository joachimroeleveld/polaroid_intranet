<?php
	include_once MODEL_PATH . 'Database.php';
	include_once dirname(__FILE__) . '/../models/template_functions.php';
?>
<div class="panel panel-default drinkers">
  <div class="panel-heading"><h3 class="panel-title">Drinkers</h3></div>
  <div class="panel-body" data-equal="div.drinker">
  	<div class="actions">
		<div class="btn-group">
		  <button type="button" class="btn btn-default add_user"><span class="glyphicon glyphicon-plus"></span> Add drinker</button>
		  <button type="button" class="btn btn-default remove_user"><span class="glyphicon glyphicon-remove"></span> Remove drinker</button>
		</div>
		<div class="clearfix"></div>
  	</div>
	<div class="drinker add">
		<div class="name">
			<span class="nameText"></span>
			<span class="glyphicon glyphicon-edit edit"></span>
			<span class="glyphicon glyphicon-remove remove"></span>
		</div>
		<div class="input-group save-name">
	  		<input type="text" class="form-control" placeholder="New name">
		 	<span class="input-group-btn">
		    	<button class="btn btn-default save" type="button">Save</button>
		  	</span>
		</div>
		<div class="profile_picture">
			<span class="btn btn-primary btn-xs btn-file upload">
				<form enctype="multipart/form-data">
		    		<span>Upload</span>
			    	<input name="profile_picture_upload" type="file">
			    </form>
			</span>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php foreach (Database::get_manager()->select('SELECT * FROM drinker ORDER BY name ASC') as $drinker): ?>
		<div class="drinker" uid="<?=$drinker['id'];?>">
			<div class="name">
				<span class="nameText"><?=$drinker['name'];?></span>
				<span class="glyphicon glyphicon-edit edit"></span>
				<span class="glyphicon glyphicon-remove remove"></span>
			</div>
			<div class="profile_picture">
				<?php if (get_profile_pic_url($drinker['id'])): ?>
					<img src="<?=get_profile_pic_url($drinker['id'], 'profile_picture_thumb');?>" alt="<?=$drinker['name'];?>" />
					<span class="btn btn-primary btn-xs btn-file change">
						<form enctype="multipart/form-data">
					    	<span>Change</span>
					    	<input name="profile_picture_change" type="file">
					    </form>
					</span>
				<?php else: ?>
					<span class="btn btn-primary btn-xs btn-file upload">
						<form enctype="multipart/form-data">
				    		<span>Upload</span>
					    	<input name="profile_picture_upload" type="file">
					    </form>
					</span>
				<?php endif ?>
			</div>
			<div class="clearfix"></div>
		</div>
	<?php endforeach ?>
  </div>
</div>