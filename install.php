<?php
	/**
	 * When this file is executed, the database scheme
	 * is installed. 
	 * Note: even if this file is accessible for every 
	 * visitor, this is not harmful since the SQL will
	 * only be executed if the database and tables do
	 * not exist yet.
	 */

	define('SITE_ROOT', dirname(__FILE__) . '/');
	require_once 'settings.php';
	require_once 'system/models/includes/constants.inc';
	require_once MODEL_PATH . 'includes/url_helper_functions.inc';
	require_once MODEL_PATH . 'Database.php';

	global $config;
	$dbname = $config['db']['main']['dbname'];

	Database::get_manager()->generic('
		CREATE DATABASE  IF NOT EXISTS `' . $dbname . '` /*!40100 DEFAULT CHARACTER SET latin1 */;
		USE `' . $dbname . '`;

		/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
		/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
		/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
		/*!40101 SET NAMES utf8 */;
		/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
		/*!40103 SET TIME_ZONE=\'+00:00\' */;
		/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
		/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
		/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=\'NO_AUTO_VALUE_ON_ZERO\' */;
		/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

		--
		-- Table structure for table `plugin`
		--

		/*!40101 SET @saved_cs_client     = @@character_set_client */;
		/*!40101 SET character_set_client = utf8 */;
		CREATE TABLE IF NOT EXISTS `plugin` (
		  `name` varchar(255) NOT NULL,
		  `status` int(11) NOT NULL,
		  PRIMARY KEY (`name`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		/*!40101 SET character_set_client = @saved_cs_client */;

		--
		-- Table structure for table `permission`
		--

		/*!40101 SET @saved_cs_client     = @@character_set_client */;
		/*!40101 SET character_set_client = utf8 */;
		CREATE TABLE IF NOT EXISTS `permission` (
		  `pid` int(11) NOT NULL AUTO_INCREMENT,
		  `name` varchar(100) NOT NULL,
		  `description` varchar(255) DEFAULT NULL,
		  `plugin` varchar(255) NOT NULL,
		  PRIMARY KEY (`pid`),
		  UNIQUE KEY `name_UNIQUE` (`name`)
		) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;
		/*!40101 SET character_set_client = @saved_cs_client */;

		--
		-- Table structure for table `user_role`
		--

		/*!40101 SET @saved_cs_client     = @@character_set_client */;
		/*!40101 SET character_set_client = utf8 */;
		CREATE TABLE IF NOT EXISTS `user_role` (
		  `uid` int(10) NOT NULL,
		  `rid` int(10) NOT NULL,
		  PRIMARY KEY (`uid`,`rid`),
		  KEY `uid_idx` (`uid`),
		  KEY `rid_idx` (`rid`),
		  CONSTRAINT `rid` FOREIGN KEY (`rid`) REFERENCES `role` (`rid`) ON DELETE CASCADE ON UPDATE CASCADE,
		  CONSTRAINT `uid` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		/*!40101 SET character_set_client = @saved_cs_client */;

		--
		-- Table structure for table `role_permission`
		--

		/*!40101 SET @saved_cs_client     = @@character_set_client */;
		/*!40101 SET character_set_client = utf8 */;
		CREATE TABLE IF NOT EXISTS `role_permission` (
		  `rid` int(11) NOT NULL,
		  `pid` int(11) NOT NULL,
		  PRIMARY KEY (`rid`,`pid`),
		  KEY `pid_idx` (`pid`),
		  CONSTRAINT `role_permission_pid` FOREIGN KEY (`pid`) REFERENCES `permission` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE,
		  CONSTRAINT `role_permission_rid` FOREIGN KEY (`rid`) REFERENCES `role` (`rid`) ON DELETE CASCADE ON UPDATE CASCADE
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		/*!40101 SET character_set_client = @saved_cs_client */;

		--
		-- Table structure for table `theme`
		--

		/*!40101 SET @saved_cs_client     = @@character_set_client */;
		/*!40101 SET character_set_client = utf8 */;
		CREATE TABLE IF NOT EXISTS `theme` (
		  `name` varchar(255) NOT NULL,
		  `status` int(11) NOT NULL,
		  PRIMARY KEY (`name`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		/*!40101 SET character_set_client = @saved_cs_client */;

		--
		-- Table structure for table `role`
		--

		/*!40101 SET @saved_cs_client     = @@character_set_client */;
		/*!40101 SET character_set_client = utf8 */;
		CREATE TABLE IF NOT EXISTS `role` (
		  `rid` int(10) NOT NULL AUTO_INCREMENT,
		  `name` varchar(64) NOT NULL,
		  PRIMARY KEY (`rid`),
		  UNIQUE KEY `name_UNIQUE` (`name`)
		) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
		/*!40101 SET character_set_client = @saved_cs_client */;

		--
		-- Table structure for table `user`
		--

		/*!40101 SET @saved_cs_client     = @@character_set_client */;
		/*!40101 SET character_set_client = utf8 */;
		CREATE TABLE IF NOT EXISTS `user` (
		  `uid` int(10) NOT NULL AUTO_INCREMENT,
		  `username` varchar(60) NOT NULL,
		  `password` varchar(128) NOT NULL,
		  PRIMARY KEY (`uid`),
		  UNIQUE KEY `username_UNIQUE` (`username`)
		) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
		/*!40101 SET character_set_client = @saved_cs_client */;
		/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

		/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
		/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
		/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
		/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
		/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
		/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
		/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
	');

	// Redirect to the login page
	header('Location: ' . BASE_URL . 'user/login', true, 303);
	exit;

