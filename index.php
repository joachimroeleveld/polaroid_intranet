<?php 
	define('SITE_ROOT', dirname(__FILE__) . '/');
	require_once 'system/models/bootstrap.php';
	require_once MODEL_PATH . 'path_handler.php';
	
	// Handle incoming requests
	handle_path(get_path());